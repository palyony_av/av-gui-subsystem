QT       += core gui

CONFIG += c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    dialogs/filtersystemdialog.cpp \
    dialogs/networkprotectiondialog.cpp \
    log/logQJson.cpp \
    main.cpp \
    mainwindow.cpp \
    module/avDllModule.cpp \
    module/logDllModule.cpp \
    registry/cloudScanRegistry.cpp \
    registry/configRegistry.cpp \
    registry/registry.cpp \
    registry/staticAnalysisRegistry.cpp \
    registry/updaterRegistry.cpp \
    tabpages/cloudScanTabPage.cpp \
    tabpages/configTabPage.cpp \
    tabpages/filtersystemtabpage.cpp \
    tabpages/logtabpage.cpp \
    tabpages/networkprotectiontabpage.cpp \
    tabpages/staticAnalysisTabPage.cpp \
    tabpages/updateTabPage.cpp \
    tabwidget.cpp \
    widget/QHexEdit/chunks.cpp \
    widget/QHexEdit/commands.cpp \
    widget/QHexEdit/qhexedit.cpp

HEADERS += \
    dialogs/filtersystemdialog.h \
    dialogs/networkprotectiondialog.h \
    log/log.h \
    log/logQJson.h \
    mainwindow.h \
    module/avDllModule.h \
    module/logDllModule.h \
    pch.h \
    registry/cloudScanRegistry.h \
    registry/configRegistry.h \
    registry/registry.h \
    registry/staticAnalysisRegistry.h \
    registry/updaterRegistry.h \
    tabpages/cloudScanTabPage.h \
    tabpages/configTabPage.h \
    tabpages/filtersystemtabpage.h \
    tabpages/logtabpage.h \
    tabpages/networkprotectiontabpage.h \
    tabpages/staticAnalysisTabPage.h \
    tabpages/updateTabPage.h \
    tabwidget.h \
    widget/QHexEdit/chunks.h \
    widget/QHexEdit/commands.h \
    widget/QHexEdit/qhexedit.h

FORMS += \
    dialogs/filtersystemdialog.ui \
    dialogs/networkprotectiondialog.ui \
    mainwindow.ui \
    tabpages/cloudScanTabPage.ui \
    tabpages/configTabPage.ui \
    tabpages/filtersystemtabpage.ui \
    tabpages/logtabpage.ui \
    tabpages/networkprotectiontabpage.ui \
    tabpages/staticAnalysisTabPage.ui \
    tabpages/updateTabPage.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
