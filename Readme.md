# To build statically you need
Download qt static build libraries from https://www.npcglib.org/~stathis/blog/precompiled-qt4-qt5/
Use https://retifrav.github.io/blog/2018/02/17/build-qt-statically/#how-to-use-static-qt to link them in project

!IMPORTANT: To configure Cloud Scan you need to manually create key-value "netaddr" in registry under the path HKEY_LOCAL_MACHINE\SOFTWARE\PalyonyAV\SandboxAnalyzer with corresponding value like " http://127.0.0.1:8080 ".
