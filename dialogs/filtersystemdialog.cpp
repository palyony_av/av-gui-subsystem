#include "filtersystemdialog.h"
#include "ui_filtersystemdialog.h"

FilterSystemDialog::FilterSystemDialog(QWidget *parent)
    : QDialog(parent), ui(new Ui::FilterSystemDialog)
{
    ui->setupUi(this);
    setWindowTitle("Add protected object");

    ui->typeComboBox->addItems({"Directory", "File", "Registry key"});
    ui->nameLineEdit->setText("\\Users\\user\\Desktop\\folder\\subfolder");
}

FilterSystemDialog::~FilterSystemDialog()
{
    delete ui;
}

void FilterSystemDialog::on_typeComboBox_currentIndexChanged(int index)
{
    enum Type { Directory = 0, File, RegistryKey };

    switch(index)
    {
    case Type::Directory:
        ui->nameLineEdit->setText("\\Users\\user\\Desktop\\folder");
        break;
    case Type::File:
        ui->nameLineEdit->setText("\\Users\\user\\Desktop\\file.txt");
        break;
    case Type::RegistryKey:
        ui->nameLineEdit->setText("\\REGISTRY\\MACHINE\\SOFTWARE\\PalyonyAV");
        break;
    }
}

void FilterSystemDialog::accept()
{
    QDialog::accept();
}

void FilterSystemDialog::reject()
{
    QDialog::reject();
}

ProtectedObject FilterSystemDialog::getData() const
{
    ProtectedObject object;

    object.type = ui->typeComboBox->currentText().toUtf8().constData();
    object.path = ui->nameLineEdit->text().toUtf8().constData();

    return object;
}
