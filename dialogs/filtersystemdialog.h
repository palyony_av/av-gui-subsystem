#ifndef FILTERSYSTEMDIALOG_H
#define FILTERSYSTEMDIALOG_H

#include "../pch.h"

#include <QDialog>
#include "registry/registry.h"

namespace Ui {
class FilterSystemDialog;
}

class FilterSystemDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FilterSystemDialog(QWidget *parent = 0);
    ~FilterSystemDialog();

    ProtectedObject getData() const;

private slots:
    void on_typeComboBox_currentIndexChanged(int index);
    void accept();
    void reject();

private:
    Ui::FilterSystemDialog *ui;
};

#endif // FILTERSYSTEMDIALOG_H
