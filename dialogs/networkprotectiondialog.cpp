#include "networkprotectiondialog.h"
#include "ui_networkprotectiondialog.h"

NetworkProtectionDialog::NetworkProtectionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NetworkProtectionDialog)
{
    ui->setupUi(this);
    setWindowTitle("Add network rule");

    ui->typeComboBox->addItems({ "Incoming", "Outcoming" });
    ui->protocolComboBox->addItems({ "ICMP", "TCP", "UDP" });
    ui->actionComboBox->addItems({ "Block", "Log" });

    ui->ipLineEdit->setValidator(new QRegExpValidator( QRegExp("([0-9]{1,3}[\\.]){3}[0-9]{1,3}") ));
    ui->portLineEdit->setValidator(new QRegExpValidator( QRegExp("[0-9]{0,5}") ));
    ui->leBytePatternOffset->setValidator(new QRegExpValidator( QRegExp("[0-9]{0,5}") ));
}

NetworkProtectionDialog::~NetworkProtectionDialog()
{
    delete ui;
}

void NetworkProtectionDialog::accept()
{
    QDialog::accept();
}

void NetworkProtectionDialog::reject()
{
    QDialog::reject();
}

NetworkRule NetworkProtectionDialog::getData() const
{
    NetworkRule rule;

    rule.type     = ui->typeComboBox->currentText().toUtf8().constData();
    rule.ip       = ui->ipLineEdit->text().toUtf8().constData();
    rule.port     = ui->portLineEdit->text().toInt();
    rule.protocol = ui->protocolComboBox->currentText().toUtf8().constData();
    rule.action   = ui->actionComboBox->currentText().toUtf8().constData();
    rule.offset   = static_cast<uint16_t>(this->ui->leBytePatternOffset->text().toUInt());

    if (this->ui->cbUseBytePattern->checkState() == Qt::Checked)
    {
        QByteArray qByteArr = ui->hexEdit->data();
        const uint8_t* begin = reinterpret_cast<uint8_t*>(qByteArr.data());
        const uint8_t* end = begin + qByteArr.length();
        rule.payload = std::vector<uint8_t>(begin, end);
    }

    return rule;
}

void NetworkProtectionDialog::on_cbUseBytePattern_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked)
    {
        this->ui->lBytePatternOffset->setEnabled(true);
        this->ui->leBytePatternOffset->setEnabled(true);
        this->ui->hexEdit->setEnabled(true);
    } else {
        this->ui->lBytePatternOffset->setEnabled(false);
        this->ui->leBytePatternOffset->setEnabled(false);
        this->ui->hexEdit->setEnabled(false);
    }
}
