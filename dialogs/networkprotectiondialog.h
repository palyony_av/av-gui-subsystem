#ifndef NETWORKPROTECTIONDIALOG_H
#define NETWORKPROTECTIONDIALOG_H

#include "../pch.h"

#include <QDialog>
#include "registry/registry.h"

namespace Ui {
class NetworkProtectionDialog;
}

class NetworkProtectionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NetworkProtectionDialog(QWidget *parent = 0);
    ~NetworkProtectionDialog();

    NetworkRule getData() const;

private slots:
    void accept();
    void reject();

    void on_cbUseBytePattern_stateChanged(int arg1);

private:
    Ui::NetworkProtectionDialog *ui;
};

#endif // NETWORKPROTECTIONDIALOG_H
