#ifndef LOG_H

#include "pch.h"
#include <string>

class Log
{
public:
    virtual std::string toJson() = 0;

    unsigned int getId()
    {
        return this->id;
    }
    const std::string& getEvent()
    {
        return this->event;
    }
    const std::string& getDate()
    {
        return this->date;
    }
    const std::string& getTime()
    {
        return this->time;
    }

    virtual ~Log() = default;

protected:
    Log(unsigned int inId,
        std::string& inEvent,
        std::string& inDate,
        std::string& inTime)
        :
        id(inId),
        event(inEvent),
        date(inDate),
        time(inTime)
    {

    }

private:
    unsigned int id;
    std::string event;
    std::string date;
    std::string time;
};

#endif // !LOG_H
