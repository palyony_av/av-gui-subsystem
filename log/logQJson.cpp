#include "logQJson.h"

std::unique_ptr<LogQJSON> LogQJSON::buildLog(const std::string& strJson)
{
    unsigned int id;
    std::string event;
    std::string date;
    std::string time;

    QJsonObject jsonObj;

    QJsonDocument doc = QJsonDocument::fromJson(
                QByteArray(strJson.c_str(), static_cast<int>(strJson.length()))
                );

    // check validity of the document
    if(doc.isNull() || !doc.isObject())
    {
        return nullptr;
    }

    jsonObj = doc.object();

    QJsonValue valueId = jsonObj["id"];
    QJsonValue valueEvent = jsonObj["event"];
    QJsonValue valueDate = jsonObj["date"];
    QJsonValue valueTime = jsonObj["time"];

    if (valueId == QJsonValue::Undefined || valueEvent == QJsonValue::Undefined
            || valueDate == QJsonValue::Undefined || valueTime == QJsonValue::Undefined)
    {
        return nullptr;
    }

    id = static_cast<unsigned int>(jsonObj["id"].toInt());
    event = jsonObj["event"].toString().toStdString();
    date = jsonObj["date"].toString().toStdString();
    time = jsonObj["time"].toString().toStdString();

    return std::unique_ptr<LogQJSON>(new LogQJSON(id, event, date, time));
}

std::unique_ptr<LogQJSON> LogQJSON::buildLog(
    unsigned int id,
    std::string event,
    std::string date,
    std::string time)
{
    return std::unique_ptr<LogQJSON>(
            new LogQJSON(id, event, date, time)
            );
}

LogQJSON::LogQJSON(unsigned int inId,
    std::string& inEvent,
    std::string& inDate,
    std::string& inTime)
    :
    super(inId, inEvent, inDate, inTime)

{

}

std::string LogQJSON::toJson()
{
    QJsonObject jsonObj; // assume this has been populated with Json data

    jsonObj["id"] = static_cast<int>(LogQJSON::super::getId());
    jsonObj["event"] = QString::fromStdString(LogQJSON::super::getEvent());
    jsonObj["date"] = QString::fromStdString(LogQJSON::super::getDate());
    jsonObj["time"] = QString::fromStdString(LogQJSON::super::getTime());

    QJsonDocument doc(jsonObj);
    return std::string(doc.toJson(QJsonDocument::Compact));
}
