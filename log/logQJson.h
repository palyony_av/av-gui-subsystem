#ifndef LOGQJSON_H
#define LOGQJSON_H

#include "pch.h"
#include "log.h"

class LogQJSON :
        public Log
{
    using super = Log;
public:
    static std::unique_ptr<LogQJSON> buildLog(const std::string& strJson);
    static std::unique_ptr<LogQJSON> buildLog(
            unsigned int id,
            std::string event,
            std::string date,
            std::string time);
    ~LogQJSON() = default;

    std::string toJson();

private:
    LogQJSON(unsigned int inId,
            std::string& inEvent,
            std::string& inDate,
            std::string& inTime);
};

#endif // !LOGQJSON_H
