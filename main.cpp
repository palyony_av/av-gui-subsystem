#include "pch.h"

#include <QMessageBox>
#include <QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QCoreApplication::addLibraryPath("./");

    QApplication a(argc, argv);
    QApplication::setStyle("Fusion");

    MainWindow w;
    w.show();

    int result = a.exec();

    return result;
}
