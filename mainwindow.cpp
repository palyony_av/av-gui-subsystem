#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "tabwidget.h"

#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    /* Initialize the tray icon, set the icon of a set of system icons,
     * As well as set a tooltip
     * */
    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setIcon(this->style()->standardIcon(QStyle::SP_ComputerIcon));
    trayIcon->setToolTip("Palyony AV");

    /* After that create a context menu of two items */
    QMenu * menu = new QMenu(this);
    QAction * viewWindow = new QAction(trUtf8("Open"), this);
    QAction * quitAction = new QAction(trUtf8("Exit"), this);

    /* connect the signals clicks on menu items to by appropriate slots.
     * The first menu item expands the application from the tray,
     * And the second menu item terminates the application
     * */
    connect(viewWindow, SIGNAL(triggered()), this, SLOT(show()));
    connect(quitAction, SIGNAL(triggered()), this, SLOT(closeMenuTrayIcon()));

    menu->addAction(viewWindow);
    menu->addAction(quitAction);

    /* Set the context menu on the icon
     * And show the application icon in the system tray
     * */
    trayIcon->setContextMenu(menu);
    trayIcon->show();

    /* Also connect clicking on the icon to the signal processor of this press
     * */
    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

/* The method that handles the closing event of the application window
 * */
void MainWindow::closeEvent(QCloseEvent * event)
{
    /* If the window is visible, and the checkbox is checked, then the completion of the application
     * Ignored, and the window simply hides that accompanied
     * The corresponding pop-up message
     */
//    if(this->isVisible())
//    {
//        event->ignore();
//        this->hide();
//        QSystemTrayIcon::MessageIcon icon = QSystemTrayIcon::MessageIcon(QSystemTrayIcon::Information);

//        trayIcon->showMessage("Palyony AV", trUtf8("Program has minimized to tray"), icon, 2000);
//    }

    QApplication::quit();
}

void MainWindow::closeMenuTrayIcon()
{
    QApplication::quit();
}

/* The method that handles click on the application icon in the system tray
 * */
void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason){
    case QSystemTrayIcon::Trigger:
    {
        /* otherwise, if the window is visible, it is hidden,
         * Conversely, if hidden, it unfolds on the screen
         * */
        if(!this->isVisible())
            this->show();
        else
            this->hide();

        break;
    }
    default:
        break;
    }
}
