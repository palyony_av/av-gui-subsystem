#include <Windows.h>

#include "avDllModule.h"

std::unique_ptr<AVDllModule> AVDllModule::buildModule(const char *pDllModulePath)
{
    func_updateModuleData fUpdateNetworkRules = nullptr;
    func_installModule fInstallDriverService = nullptr;
    func_uninstallModule fStopDriverService = nullptr;
    HMODULE hDllModule = nullptr;

    hDllModule = LoadLibraryA(pDllModulePath);
    if (hDllModule == nullptr)
    {
        return nullptr;
    }

    fUpdateNetworkRules = reinterpret_cast<func_updateModuleData>(
                    GetProcAddress(hDllModule, FUNC_UPDATE_MODULE_DATA)
                );
    fInstallDriverService = reinterpret_cast<func_installModule>(
                    GetProcAddress(hDllModule, FUNC_INSTALL_MODULE)
                );
    fStopDriverService = reinterpret_cast<func_uninstallModule>(
                    GetProcAddress(hDllModule, FUNC_UNINSTALL_MODULE)
                );

    if (fUpdateNetworkRules == nullptr || fInstallDriverService == nullptr
            || fStopDriverService == nullptr)
    {
        return nullptr;
    }

    return std::unique_ptr<AVDllModule>(
                new AVDllModule(hDllModule,
                                fUpdateNetworkRules,
                                fInstallDriverService,
                                fStopDriverService)
                );
}

AVDllModule::AVDllModule(
        HMODULE hOpenedDllModule,
        func_updateModuleData funcUpdateNetworkRules,
        func_installModule funcInstallDriverService,
        func_uninstallModule funcStopDriverService)
    :
      fUpdateModuleData(funcUpdateNetworkRules),
      fInstallModule(funcInstallDriverService),
      fUninstallModule(funcStopDriverService),
      hAvModule(nullptr),
      hDllModule(hOpenedDllModule)
{

}

AVDllModule::~AVDllModule()
{
    this->uninstallModule();
    FreeLibrary(this->hDllModule);
}

bool AVDllModule::installModule()
{
    if (this->hAvModule != nullptr)
    {
        return false;
    }

    this->hAvModule = this->fInstallModule();
    if (this->hAvModule == nullptr)
    {
        return false;
    }

    return true;
}

bool AVDllModule::updateModuleData()
{
    if (this->hAvModule == nullptr)
    {
        return false;
    }

    if (this->fUpdateModuleData(this->hAvModule) == AV_STATUS_FAIL)
    {
        return false;
    }

    return true;
}

bool AVDllModule::uninstallModule()
{
    if (this->hAvModule == nullptr)
    {
        return false;
    }

    if (this->fUninstallModule(this->hAvModule) == AV_STATUS_FAIL)
    {
        return false;
    }
    this->hAvModule = nullptr;

    return true;
}
