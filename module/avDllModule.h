#ifndef DLLTHREADSTORE_H
#define DLLTHREADSTORE_H

#include "pch.h"

/* Store path for all dll modules here */
#define DLL_NETWORK_MODULE_NAME "net-filter-dll.dll"
#define DLL_NETWORK_MODULE_PATH "lib/" DLL_NETWORK_MODULE_NAME

#define DLL_CLOUD_SCAN_MODULE_NAME "CuckooSandboxDll.dll"
#define DLL_CLOUD_SCAN_MODULE_PATH "lib/" DLL_CLOUD_SCAN_MODULE_NAME

#define DLL_FILTER_MODULE_NAME "FilterSubsystem.dll"
#define DLL_FILTER_MODULE_PATH "lib/" DLL_FILTER_MODULE_NAME

#define DLL_REAL_TIME_PROTECTION_MODULE_NAME "RealTimeProtection.dll"
#define DLL_REAL_TIME_PROTECTION_MODULE_PATH "lib/" DLL_REAL_TIME_PROTECTION_MODULE_NAME

#define DLL_STATIC_ANALYSIS_NAME "Static_Analysis_Subsystem.dll"
#define DLL_STATIC_ANALYSIS_PATH "lib/" DLL_STATIC_ANALYSIS_NAME

#define DLL_UPDATER_MODULE_NAME "updater-dll.dll"
#define DLL_UPDATER_MODULE_PATH "lib/" DLL_UPDATER_MODULE_NAME

#define FUNC_UPDATE_MODULE_DATA "updateModuleData"
#define FUNC_INSTALL_MODULE "installModule"
#define FUNC_UNINSTALL_MODULE "uninstallModule"

#define AV_STATUS_SUCCESS 1
#define AV_STATUS_FAIL 0

typedef void* AVHMODULE;

typedef AVHMODULE (*func_installModule)();
typedef unsigned int (*func_updateModuleData)(AVHMODULE);
typedef unsigned int (*func_uninstallModule)(AVHMODULE);

class AVDllModule
{
public:
    static std::unique_ptr<AVDllModule> buildModule(const char *pDllModulePath);

    AVDllModule(const AVDllModule&)= delete;
    AVDllModule& operator=(const AVDllModule&)= delete;
    ~AVDllModule();

    bool installModule();
    bool updateModuleData();
    bool uninstallModule();

private:
    AVDllModule(HMODULE hOpenedDllModule,
                func_updateModuleData funcUpdateModuleData,
                func_installModule funcInstallModule,
                func_uninstallModule funcStopModule);

    func_updateModuleData fUpdateModuleData;
    func_installModule fInstallModule;
    func_uninstallModule fUninstallModule;

    AVHMODULE hAvModule;
    HMODULE hDllModule;
};

#endif // DLLTHREADSTORE_H
