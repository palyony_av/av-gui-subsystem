#include "logDllModule.h"

std::unique_ptr<LogDllModule> LogDllModule::buildModule(const char *pDllModulePath)
{
    func_getAllLogEntries fGetAllLogEntries = nullptr;
    HMODULE hDllModule = nullptr;

    hDllModule = LoadLibraryA(pDllModulePath);
    if (hDllModule == nullptr)
    {
        return nullptr;
    }

    fGetAllLogEntries = reinterpret_cast<func_getAllLogEntries>(
                    GetProcAddress(hDllModule, FUNC_GET_ALL_LOG_ENTRIES)
                );

    if (fGetAllLogEntries == nullptr)
    {
        return nullptr;
    }

    return std::unique_ptr<LogDllModule>(
                new LogDllModule(hDllModule,
                                fGetAllLogEntries)
                );
}

LogDllModule::LogDllModule(HMODULE hOpenedLogLib,
                           func_getAllLogEntries funcGetAllLogEntries)
    :
      fGetAllLogEntries(funcGetAllLogEntries),
      hDllModule(hOpenedLogLib)
{

}

LogDllModule::~LogDllModule()
{
    FreeLibrary(this->hDllModule);
}
