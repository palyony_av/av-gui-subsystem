#ifndef LOGMODULECONTEXT_H
#define LOGMODULECONTEXT_H

#include "pch.h"
#include "../log/logQJson.h"

/* Store path for all dll modules here */
#define DLL_LOG_MODULE_NAME "Logger.dll"
#define DLL_LOG_MODULE_PATH "lib/" DLL_LOG_MODULE_NAME

#define FUNC_GET_ALL_LOG_ENTRIES "getAllLogEntries"

#define AV_NOT_ENOUGH_MEMORY 2
#define AV_ERRONEOUS_INPUT 1
#define AV_SUCCESS 0

#define DEFAULT_LOG_SIZE 512

typedef uint32_t (*func_getAllLogEntries)(void *pBuf, uint32_t bufSize, uint32_t* pUsedSize);

class LogDllModule
{
public:
    static std::unique_ptr<LogDllModule> buildModule(const char *pDllModulePath);

    ~LogDllModule();
    LogDllModule(const LogDllModule&)= delete;
    LogDllModule& operator=(const LogDllModule&)= delete;

    func_getAllLogEntries fGetAllLogEntries;

    // TODO: Add threads to checks logs

private:
    LogDllModule(HMODULE hOpenedLogLib,
            func_getAllLogEntries funcGetAllLogEntries);

    HMODULE hDllModule;
};

#endif // LOGMODULECONTEXT_H
