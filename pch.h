#ifndef PCH_H
#define PCH_H

#include <Windows.h>
#include <memory>
#include <string>
#include <vector>
#include <functional>
#include <thread>
#include <forward_list>
#include <stdexcept>
#include <atomic>

#include <QJsonObject>
#include <QJsonDocument>
#include <QMessageBox>
#include <QStandardItemModel>
#include <QStandardItem>

#endif // PCH_H
