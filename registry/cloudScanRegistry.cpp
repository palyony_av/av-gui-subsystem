#include <QMessageBox>
#include <QString>

#include "cloudScanRegistry.h"

#define MAX_KEY_LENGTH 255
#define MAX_PATH 260

#pragma comment(lib, "Advapi32.lib")

std::unique_ptr<CloudScanRegistry> CloudScanRegistry::build()
{
    LSTATUS retCode;
    HKEY hKey = nullptr;
    DWORD dispozition;

    retCode = RegCreateKeyExA(
                HKEY_LOCAL_MACHINE,     // Handle of the parent registry key.
                AV_CLOUD_SCAN_REG_KEY,  // Name of the new key to open or create.
                0,                      // Reserved, pass 0.
                nullptr,                // The user-defined class type of this key.
                0,                      // Flags controlling the key creation
                KEY_ALL_ACCESS,         // Access level desired.
                nullptr,                // Security attributes for the key.
                &hKey,                  // Destination for the resulting handle.
                &dispozition            // Receives REG_CREATED_NEW_KEY or REG_OPENED_EXISTING_KEY.
    );
    if (retCode != ERROR_SUCCESS)
    {
        return nullptr;
    }

    if (dispozition == REG_CREATED_NEW_KEY)
    {
        /* Set ip address of the remote server */
        retCode = RegSetValueExA(
                    hKey,
                    AV_CLOUD_SCAN_NETADDR,
                    0,
                    REG_SZ,
                    reinterpret_cast<const BYTE *>(AV_CLOUD_SCAN_NETADDR_VERSION),
                    sizeof(AV_CLOUD_SCAN_NETADDR_VERSION)
        );
        if (retCode != ERROR_SUCCESS)
        {
            return nullptr;
        }
    }

    return std::unique_ptr<CloudScanRegistry>(new CloudScanRegistry(hKey));
}

bool CloudScanRegistry::addObjToAnalyze(const CloudScanObject &obj)
{
    LSTATUS retCode = 0;

    DWORD cSubKeys = getSubkeyCount(retCode);
    if (retCode != ERROR_SUCCESS)
    {
        QMessageBox::warning(nullptr, "CloudScanRegistry::addObjToAnalyze",
                             "Error: getSubkeyCount");
        return false;
    }

    cSubKeys += 1;
    bool result = true;
    HKEY hSubkey = nullptr;
    std::string subkeyName = std::to_string(cSubKeys);

    retCode = RegCreateKeyExA(
                this->hKey,                           // Handle of the parent registry key.
                subkeyName.c_str(),                   // Name of the new key to open or create.
                0,                                    // Reserved, pass 0.
                nullptr,                              // The user-defined class type of this key.
                0,                                    // Flags controlling the key creation
                KEY_ALL_ACCESS,                       // Access level desired.
                nullptr,                              // Security attributes for the key.
                &hSubkey,                             // Destination for the resulting handle.
                nullptr                               // Receives REG_CREATED_NEW_KEY or REG_OPENED_EXISTING_KEY.
    );
    if (retCode != ERROR_SUCCESS)
    {
        QMessageBox::warning(nullptr, "CloudScanRegistry::addObjToAnalyze",
                             "failed to open");
        result = false;
        goto Exit;
    }

    retCode = RegSetValueExA(
        hSubkey,
        AV_CLOUD_SCAN_MODULE_PATH,
        0,
        REG_SZ,
        reinterpret_cast<const BYTE *>(obj.filePath.c_str()),
        static_cast<DWORD>(obj.filePath.length() + 1)
    );
    if (retCode != ERROR_SUCCESS)
    {
        result = false;
        goto Exit;
    }

    retCode = RegSetValueExA(
        hSubkey,
        AV_CLOUD_SCAN_MODULE_STATE,
        0,
        REG_DWORD,
        reinterpret_cast<const BYTE *>(&obj.state),
        sizeof(unsigned int)
    );
    if (retCode != ERROR_SUCCESS)
    {
        result = false;
        goto Exit;
    }

    retCode = RegSetValueExA(
        hSubkey,
        AV_CLOUD_SCAN_MODULE_ANSWER,
        0,
        REG_DWORD,
        reinterpret_cast<const BYTE *>(&obj.answer),
        sizeof(int)
    );
    if (retCode != ERROR_SUCCESS)
    {
        result = false;
        goto Exit;
    }
    result = true;

Exit:
    if (hSubkey != nullptr)
    {
        CloseHandle(hSubkey);
    }

    if (result == false)
    {
        RegDeleteKeyExA(this->hKey, subkeyName.c_str(), KEY_WOW64_64KEY, 0);
    }

    return result;
}

void CloudScanRegistry::delObjToAnalyze(uint32_t objectId)
{
    /* In GUI key id start from 0, but in registry from 1 */
    RegDeleteKeyExA(this->hKey, std::to_string(objectId + 1).c_str(), KEY_WOW64_64KEY, 0);
}

std::unique_ptr<std::forward_list<CloudScanObject> > CloudScanRegistry::getAnalyzeObjects()
{
    LSTATUS retCode = 0;

    DWORD cSubKeys = getSubkeyCount(retCode);
    if (retCode != ERROR_SUCCESS)
    {
        QMessageBox::warning(nullptr, "CloudScanRegistry::getAnalyzeObjects",
                             "Could not get subkey count");
        return nullptr;
    }

    std::unique_ptr<std::forward_list<CloudScanObject> > pResultList =
        std::unique_ptr<std::forward_list<CloudScanObject>>(
            new std::forward_list<CloudScanObject>
    );
    char achKey[MAX_KEY_LENGTH];   // buffer for subkey name
    char achValue[MAX_PATH];
    int iStateValue;
    int iAnswer;
    auto forwardBeginIt = pResultList->end();
    auto forwardEndIt = pResultList->end();

    /* because of usage forward_list the */
    for (DWORD i = 0; i < cSubKeys; i++)
    {
        /* Get subkey name */
        DWORD cbName = MAX_KEY_LENGTH;
        retCode = RegEnumKeyExA(
            this->hKey,
            i,
            reinterpret_cast<char *>(&achKey),
            &cbName,
            nullptr,
            nullptr,
            nullptr,
            nullptr
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        /* Get path value */
        DWORD cchValue = MAX_PATH;
        retCode = RegGetValueA(
            this->hKey,
            achKey,
            AV_CLOUD_SCAN_MODULE_PATH,
            RRF_RT_ANY,
            nullptr,
            achValue,
            &cchValue
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        /* Get state value */
        DWORD intValueSize = sizeof(int);
        retCode = RegGetValueA(
            this->hKey,
            achKey,
            AV_CLOUD_SCAN_MODULE_STATE,
            RRF_RT_ANY,
            nullptr,
            reinterpret_cast<char *>(&iStateValue),
            &intValueSize
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        /* Get answer value */
        retCode = RegGetValueA(
            this->hKey,
            achKey,
            AV_CLOUD_SCAN_MODULE_ANSWER,
            RRF_RT_ANY,
            nullptr,
            reinterpret_cast<char *>(&iAnswer),
            &intValueSize
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        if (forwardBeginIt != forwardEndIt)
        {
            forwardBeginIt = pResultList->insert_after(forwardBeginIt, CloudScanObject(achValue, cchValue, iStateValue, iAnswer));
        } else {
            pResultList->push_front(CloudScanObject(achValue, cchValue, iStateValue, iAnswer));
            forwardBeginIt = pResultList->begin();
        }
    }

    return pResultList;
}

CloudScanRegistry::CloudScanRegistry(HKEY hInKey)
    : hKey(hInKey)
{

}

CloudScanRegistry::~CloudScanRegistry()
{
    CloseHandle(this->hKey);
}

unsigned long CloudScanRegistry::getSubkeyCount(long & retCode)
{
    unsigned long cSubKeys;
    retCode = RegQueryInfoKeyA(
            this->hKey,              // key handle
            nullptr,                 // buffer for class name
            nullptr,                 // size of class string
            nullptr,                 // reserved
            &cSubKeys,               // number of subkeys
            nullptr,                 // longest subkey size
            nullptr,                 // longest class string
            nullptr,                 // number of values for this key
            nullptr,                 // longest value name
            nullptr,                 // longest value data
            nullptr,                 // security descriptor
            nullptr                  // last write time
    );
    return cSubKeys;
}
