#ifndef SANDBOXREGISTRY_H
#define SANDBOXREGISTRY_H

#include <Windows.h>
#include <string>
#include <memory>
#include <forward_list>

#define AV_CLOUD_SCAN_REG_KEY "SOFTWARE\\PalyonyAV\\CloudScan"
#define AV_CLOUD_SCAN_MODULE_PATH "path"
#define AV_CLOUD_SCAN_MODULE_STATE "state"
#define AV_CLOUD_SCAN_MODULE_ANSWER "answer"

#define AV_CLOUD_SCAN_VERSION "version"
#define AV_CLOUD_SCAN_NETADDR "netaddr"

#define AV_CLOUD_SCAN_VERSION_VALUE "0.0.0"
#define AV_CLOUD_SCAN_NETADDR_VERSION "http://127.0.0.1:8080"


enum class CloudScanAnalyzerState: unsigned int
{
    waitForAnalysis = 0,
    analyseIsRunning = 1,
    analyseIsEnded = 2,
    analyseIsFailed = 3
};

struct CloudScanObject
{
    CloudScanObject(const char *pPath, unsigned long pathSize, int inState, int inAnswer)
    {
        this->filePath = std::string(pPath, pathSize);
        this->state = static_cast<CloudScanAnalyzerState>(inState);
        this->answer = inAnswer;
    }

    CloudScanObject(const std::string &inFilePath, CloudScanAnalyzerState inState, int inAnswer)
        : filePath(inFilePath), state(inState), answer(inAnswer)
    {

    }

    std::string filePath;
    CloudScanAnalyzerState state;
    int answer;
};

class CloudScanRegistry
{
public:
    static std::unique_ptr<CloudScanRegistry> build();
    ~CloudScanRegistry();

    bool addObjToAnalyze(const CloudScanObject &obj);
    void delObjToAnalyze(uint32_t objectId);
    std::unique_ptr<std::forward_list<CloudScanObject>> getAnalyzeObjects();

private:
    HKEY hKey;
    CloudScanRegistry(HKEY hInKey);
    unsigned long getSubkeyCount(long & retCode);
};

#endif // SANDBOXREGISTRY_H
