#include "configRegistry.h"

std::unique_ptr<ConfigRegistry> ConfigRegistry::build()
{
    HKEY hCloudScan = nullptr;
    HKEY hStaticAnalysis = nullptr;
    HKEY hUpdater = nullptr;
    LSTATUS retCode;

    retCode = RegCreateKeyExA(
                HKEY_LOCAL_MACHINE,     // Handle of the parent registry key.
                AV_CLOUD_SCAN_REG_KEY,  // Name of the new key to open or create.
                0,                      // Reserved, pass 0.
                nullptr,                // The user-defined class type of this key.
                0,                      // Flags controlling the key creation
                KEY_ALL_ACCESS,         // Access level desired.
                nullptr,                // Security attributes for the key.
                &hCloudScan,            // Destination for the resulting handle.
                nullptr                 // Receives REG_CREATED_NEW_KEY or REG_OPENED_EXISTING_KEY.
    );
    if (retCode != ERROR_SUCCESS)
    {
        goto Exit;
    }

    retCode = RegCreateKeyExA(
                HKEY_LOCAL_MACHINE,     // Handle of the parent registry key.
                AV_STATIC_ANAL_REG_KEY, // Name of the new key to open or create.
                0,                      // Reserved, pass 0.
                nullptr,                // The user-defined class type of this key.
                0,                      // Flags controlling the key creation
                KEY_ALL_ACCESS,         // Access level desired.
                nullptr,                // Security attributes for the key.
                &hStaticAnalysis,       // Destination for the resulting handle.
                nullptr                 // Receives REG_CREATED_NEW_KEY or REG_OPENED_EXISTING_KEY.
    );
    if (retCode != ERROR_SUCCESS)
    {
        goto Exit;
    }

    retCode = RegCreateKeyExA(
                HKEY_LOCAL_MACHINE,     // Handle of the parent registry key.
                AV_UPDATER_REG_KEY,     // Name of the new key to open or create.
                0,                      // Reserved, pass 0.
                nullptr,                // The user-defined class type of this key.
                0,                      // Flags controlling the key creation
                KEY_ALL_ACCESS,         // Access level desired.
                nullptr,                // Security attributes for the key.
                &hUpdater,              // Destination for the resulting handle.
                nullptr                 // Receives REG_CREATED_NEW_KEY or REG_OPENED_EXISTING_KEY.
    );
    if (retCode != ERROR_SUCCESS)
    {
        goto Exit;
    }

    return std::unique_ptr<ConfigRegistry>(new ConfigRegistry(hCloudScan, hStaticAnalysis, hUpdater));

Exit:
    if (hCloudScan != nullptr)
    {
        CloseHandle(hCloudScan);
    }

    if (hStaticAnalysis != nullptr)
    {
        CloseHandle(hStaticAnalysis);
    }

    if (hUpdater != nullptr)
    {
        CloseHandle(hUpdater);
    }

    return nullptr;
}

ConfigRegistry::~ConfigRegistry()
{
    CloseHandle(this->hCloudScan);
    CloseHandle(this->hStaticAnalysis);
    CloseHandle(this->hUpdater);
}

std::pair<std::string, std::string> ConfigRegistry::getCloudScanNetAddr()
{
    char achValue[this->MAX_KEY_LENGTH];
    DWORD cchValue = this->MAX_KEY_LENGTH;

    LSTATUS retCode = RegGetValueA(
                HKEY_LOCAL_MACHINE,
                AV_CLOUD_SCAN_REG_KEY,
                AV_NETADDR_KEY,
                RRF_RT_REG_SZ,
                nullptr,
                achValue,
                &cchValue
    );
    if (retCode != ERROR_SUCCESS)
    {
        return std::pair<std::string, std::string> (std::string(), std::string());
    } else {
        std::string netAddr(achValue, cchValue - 1);
        size_t foundStart = netAddr.find("http://");
        if (foundStart == std::string::npos)
        {
            return std::pair<std::string, std::string> (std::string(), std::string());
        }
        size_t offset = sizeof ("http://") - 1 + foundStart;
        size_t foundEnd = netAddr.find(':', offset);
        if (foundEnd == std::string::npos)
        {
            return std::pair<std::string, std::string> (std::string(), std::string());
        }

        return std::pair<std::string, std::string> (
                    netAddr.substr(offset, foundEnd - offset),
                    netAddr.substr(foundEnd + 1, netAddr.length() - (foundEnd + 1))
        );
    }
}

std::pair<std::string, std::string> ConfigRegistry::getUpdateNetAddr()
{
    char achValue[this->MAX_KEY_LENGTH];
    DWORD cchValue = this->MAX_KEY_LENGTH;

    LSTATUS retCode = RegGetValueA(
                HKEY_LOCAL_MACHINE,
                AV_UPDATER_REG_KEY,
                AV_NETADDR_KEY,
                RRF_RT_REG_SZ,
                nullptr,
                achValue,
                &cchValue
    );
    if (retCode != ERROR_SUCCESS)
    {
        return std::pair<std::string, std::string> (std::string(), std::string());
    } else {
        std::string netAddr(achValue, cchValue - 1);
        size_t foundStart = netAddr.find("http://");
        if (foundStart == std::string::npos)
        {
            return std::pair<std::string, std::string> (std::string(), std::string());
        }
        size_t offset = sizeof ("http://") - 1 + foundStart;
        size_t foundEnd = netAddr.find(':', offset);
        if (foundEnd == std::string::npos)
        {
            return std::pair<std::string, std::string> (std::string(), std::string());
        }

        return std::pair<std::string, std::string> (
                    netAddr.substr(offset, foundEnd - offset),
                    netAddr.substr(foundEnd + 1, netAddr.length() - (foundEnd + 1))
        );
    }
}

std::tuple<std::string, std::string, std::string, std::string> ConfigRegistry::getStaticAnalysisPath()
{
    char achValue[MAX_PATH];
    DWORD cchValue = MAX_PATH;
    LSTATUS retCode;

    retCode = RegGetValueA(
                HKEY_LOCAL_MACHINE,
                AV_STATIC_ANAL_REG_KEY,
                AV_STATIC_ANAL_CLAM_AV_DB_PATH,
                RRF_RT_REG_SZ,
                nullptr,
                achValue,
                &cchValue
    );
    if (retCode != ERROR_SUCCESS)
    {
        return std::make_tuple(std::string(), std::string(), std::string(), std::string());
    }
    std::string clamAVDBPath(achValue, cchValue - 1);
    cchValue = MAX_PATH;

    retCode = RegGetValueA(
                HKEY_LOCAL_MACHINE,
                AV_STATIC_ANAL_REG_KEY,
                AV_STATIC_ANAL_LEVEL_DB_PATH,
                RRF_RT_REG_SZ,
                nullptr,
                achValue,
                &cchValue
    );
    if (retCode != ERROR_SUCCESS)
    {
        return std::make_tuple(std::string(), std::string(), std::string(), std::string());
    }
    std::string levelDBPath(achValue, cchValue - 1);
    cchValue = MAX_PATH;

    retCode = RegGetValueA(
                HKEY_LOCAL_MACHINE,
                AV_STATIC_ANAL_REG_KEY,
                AV_STATIC_ANAL_YARA_RULES_MALW_PATH,
                RRF_RT_REG_SZ,
                nullptr,
                achValue,
                &cchValue
    );
    if (retCode != ERROR_SUCCESS)
    {
        return std::make_tuple(std::string(), std::string(), std::string(), std::string());
    }
    std::string yaraRulesMalwPath(achValue, cchValue - 1);
    cchValue = MAX_PATH;

    retCode = RegGetValueA(
                HKEY_LOCAL_MACHINE,
                AV_STATIC_ANAL_REG_KEY,
                AV_STATIC_ANAL_YARA_RULES_PACK_PATH,
                RRF_RT_REG_SZ,
                nullptr,
                achValue,
                &cchValue
    );
    if (retCode != ERROR_SUCCESS)
    {
        return std::make_tuple(std::string(), std::string(), std::string(), std::string());
    }
    std::string yaraRulesPackPath(achValue, cchValue - 1);

    return std::make_tuple(clamAVDBPath, levelDBPath, yaraRulesMalwPath, yaraRulesPackPath);
}

bool ConfigRegistry::setCloudScanNetAddr(std::string ipAddr, std::string port)
{
    LSTATUS retCode;
    std::string netAddr = "http://" + ipAddr + ':' + port;

    retCode = RegSetValueExA(
                this->hCloudScan,
                AV_NETADDR_KEY,
                0,
                REG_SZ,
                reinterpret_cast<const BYTE *>(netAddr.c_str()),
                static_cast<DWORD>(netAddr.length() + 1)
    );
    if (retCode != ERROR_SUCCESS)
    {
        return false;
    } else {
        return true;
    }
}

bool ConfigRegistry::setUpdateNetAddr(std::string ipAddr, std::string port)
{
    LSTATUS retCode;
    std::string netAddr = "http://" + ipAddr + ':' + port;

    retCode = RegSetValueExA(
                this->hUpdater,
                AV_NETADDR_KEY,
                0,
                REG_SZ,
                reinterpret_cast<const BYTE *>(netAddr.c_str()),
                static_cast<DWORD>(netAddr.length() + 1)
    );
    if (retCode != ERROR_SUCCESS)
    {
        return false;
    } else {
        return true;
    }
}

bool ConfigRegistry::setStaticAnalysisPath(std::string clamAVDBPath, std::string levelDBPath,
                                           std::string yaraRulesMalwPath, std::string yaraRulesPackPath)
{
    LSTATUS retCode;

    retCode = RegSetValueExA(
                this->hStaticAnalysis,
                AV_STATIC_ANAL_CLAM_AV_DB_PATH,
                0,
                REG_SZ,
                reinterpret_cast<const BYTE *>(clamAVDBPath.c_str()),
                static_cast<DWORD>(clamAVDBPath.length() + 1)
    );
    if (retCode != ERROR_SUCCESS)
    {
        return false;
    }

    retCode = RegSetValueExA(
                this->hStaticAnalysis,
                AV_STATIC_ANAL_LEVEL_DB_PATH,
                0,
                REG_SZ,
                reinterpret_cast<const BYTE *>(levelDBPath.c_str()),
                static_cast<DWORD>(levelDBPath.length() + 1)
    );
    if (retCode != ERROR_SUCCESS)
    {
        return false;
    }

    retCode = RegSetValueExA(
                this->hStaticAnalysis,
                AV_STATIC_ANAL_YARA_RULES_MALW_PATH,
                0,
                REG_SZ,
                reinterpret_cast<const BYTE *>(yaraRulesMalwPath.c_str()),
                static_cast<DWORD>(yaraRulesMalwPath.length() + 1)
    );
    if (retCode != ERROR_SUCCESS)
    {
        return false;
    }

    retCode = RegSetValueExA(
                this->hStaticAnalysis,
                AV_STATIC_ANAL_YARA_RULES_PACK_PATH,
                0,
                REG_SZ,
                reinterpret_cast<const BYTE *>(yaraRulesPackPath.c_str()),
                static_cast<DWORD>(yaraRulesPackPath.length() + 1)
    );
    if (retCode != ERROR_SUCCESS)
    {
        return false;
    }

    return true;
}

ConfigRegistry::ConfigRegistry(HKEY hInCloudScan, HKEY hInStaticAnalysis, HKEY hInUpdater)
    :hCloudScan(hInCloudScan), hStaticAnalysis(hInStaticAnalysis), hUpdater(hInUpdater)
{

}


