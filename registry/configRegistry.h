#ifndef CONFIGREGISTRY_H
#define CONFIGREGISTRY_H

#include <Windows.h>
#include <memory>
#include <string>
#include <tuple>

#define AV_CLOUD_SCAN_REG_KEY "SOFTWARE\\PalyonyAV\\CloudScan"
#define AV_STATIC_ANAL_REG_KEY "SOFTWARE\\PalyonyAV\\StaticAnal"
#define AV_UPDATER_REG_KEY "SOFTWARE\\PalyonyAV\\Updater"

#define AV_NETADDR_KEY "netaddr"
#define AV_STATIC_ANAL_CLAM_AV_DB_PATH "ClamAVDBPath"
#define AV_STATIC_ANAL_LEVEL_DB_PATH "LevelDBPath"
#define AV_STATIC_ANAL_YARA_RULES_MALW_PATH "YaraRulesMalwPath"
#define AV_STATIC_ANAL_YARA_RULES_PACK_PATH "YaraRulesPackPath"

class ConfigRegistry
{
public:
    static std::unique_ptr<ConfigRegistry> build();
    ~ConfigRegistry();

    std::pair<std::string, std::string> getCloudScanNetAddr();
    std::pair<std::string, std::string> getUpdateNetAddr();
    std::tuple<std::string, std::string, std::string, std::string> getStaticAnalysisPath();

    bool setCloudScanNetAddr(std::string ipAddr, std::string port);
    bool setUpdateNetAddr(std::string ipAddr, std::string port);
    bool setStaticAnalysisPath(std::string clamAVDBPath, std::string levelDBPath,
                               std::string yaraRulesMalwPath, std::string yaraRulesPackPath);

private:
    ConfigRegistry(HKEY hInCloudScan, HKEY hInStaticAnalysis, HKEY hInUpdater);

    HKEY hCloudScan;
    HKEY hStaticAnalysis;
    HKEY hUpdater;

    static constexpr int MAX_KEY_LENGTH = 255;
};

#endif // CONFIGREGISTRY_H
