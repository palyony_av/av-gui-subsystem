#include "registry.h"

#include <exception>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QMessageBox>

#define AV_ROOT_REG_KEY    "SOFTWARE\\PalyonyAV"
#define AV_FILTER_REG_KEY  "SOFTWARE\\PalyonyAV\\FilterSubsystem"
#define AV_NETWORK_REG_KEY "SOFTWARE\\PalyonyAV\\NetworkProtection"

#define MAX_VALUE_NAME 1024

#pragma comment(lib, "Advapi32.lib")

Registry::Registry() :
    m_hRootKey(nullptr),
    m_hFilterKey(nullptr),
    m_hNetworkKey(nullptr)
{
    DWORD retCode;

    retCode = RegCreateKeyExA(HKEY_LOCAL_MACHINE, AV_ROOT_REG_KEY, 0, NULL, 0, KEY_ALL_ACCESS, NULL, &this->m_hRootKey, NULL);

    if (retCode != ERROR_SUCCESS)
    {
        const std::string errorMessage = (retCode == ERROR_ACCESS_DENIED) ?
                    "Admin rights are required" :
                    std::string("RegCreateKeyEx failed: ") + std::to_string(retCode);

        throw std::runtime_error(errorMessage);
    }

    retCode = RegCreateKeyExA(HKEY_LOCAL_MACHINE, AV_FILTER_REG_KEY, 0, NULL, 0, KEY_ALL_ACCESS, NULL, &this->m_hFilterKey, NULL);

    if (retCode != ERROR_SUCCESS)
    {
        const std::string errorMessage = std::string("RegCreateKeyEx failed: ") + std::to_string(retCode);
        throw std::runtime_error(errorMessage);
    }

    retCode = RegCreateKeyExA(HKEY_LOCAL_MACHINE, AV_NETWORK_REG_KEY, 0, NULL, 0, KEY_ALL_ACCESS, NULL, &this->m_hNetworkKey, NULL);

    if (retCode != ERROR_SUCCESS)
    {
        const std::string errorMessage = std::string("RegCreateKeyEx failed: ") + std::to_string(retCode);
        throw std::runtime_error(errorMessage);
    }
}

Registry::~Registry()
{
    if (this->m_hRootKey)
        RegCloseKey(this->m_hRootKey);

    if (this->m_hFilterKey)
        RegCloseKey(this->m_hFilterKey);

    if (this->m_hNetworkKey)
        RegCloseKey(this->m_hNetworkKey);
}

Registry* Registry::getInstance()
{
    try
    {
        static Registry reg;
        return &reg;
    }
    catch (const std::exception& /*e*/)
    {
        throw;
    }
}

std::shared_ptr<std::vector<ProtectedObject>> Registry::getObjectsToProtect()
{
    protectedObjectIds.clear();

    auto objects = std::make_shared<std::vector<ProtectedObject>>();

    DWORD nValues;
    DWORD i, retCode;

    CHAR achValue[MAX_VALUE_NAME];
    DWORD cchValue = MAX_VALUE_NAME;

    retCode = RegQueryInfoKey(this->m_hFilterKey, NULL, NULL, NULL, NULL, NULL, NULL, &nValues, NULL, NULL, NULL, NULL);

    if (retCode == ERROR_SUCCESS && nValues > 0)
    {
        for (i = 0, retCode = ERROR_SUCCESS; i < nValues; i++)
        {
            cchValue = MAX_VALUE_NAME;
            achValue[0] = '\0';
            retCode = RegEnumValueA(this->m_hFilterKey, i, achValue, &cchValue, NULL, NULL, NULL, NULL);

            if (retCode == ERROR_SUCCESS)
            {
                char valueBuffer[MAX_VALUE_NAME];
                DWORD bufferSize = sizeof(valueBuffer);

                retCode = RegGetValueA(HKEY_LOCAL_MACHINE, AV_FILTER_REG_KEY, achValue, RRF_RT_ANY, NULL, &valueBuffer, &bufferSize);

                if (retCode == ERROR_SUCCESS)
                {
                    QString objectJsonString {QString::fromUtf8(valueBuffer)};

                    QJsonObject objectJsonObject = QJsonDocument::fromJson(objectJsonString.toUtf8()).object();

                    ProtectedObject object;

                    object.id   = achValue;
                    object.type = objectJsonObject["type"].toString().toUtf8().constData();
                    object.path = objectJsonObject["path"].toString().toUtf8().constData();

                    objects->push_back(object);
                    protectedObjectIds.push_back(std::stoi(achValue));
                }
            }
        }
    }

    return objects;
}

bool Registry::addObjectToProtect(const ProtectedObject& object)
{
    QJsonObject objectJsonObject;
    objectJsonObject["type"] = QString::fromUtf8(object.type.c_str());
    objectJsonObject["path"] = QString::fromUtf8(object.path.c_str());

    QString objectJsonString(QJsonDocument{objectJsonObject}.toJson(QJsonDocument::Compact));

    int newId = protectedObjectIds.empty() ?
                1 : *max_element(protectedObjectIds.begin(), protectedObjectIds.end()) + 1;

    std::string key {std::to_string(newId)};
    std::string data {objectJsonString.toUtf8().constData()};

    DWORD retCode = RegSetValueExA(this->m_hFilterKey, key.c_str(), 0, REG_SZ,
        reinterpret_cast<const BYTE*>(data.c_str()), static_cast<DWORD>(data.size()));

    return retCode == ERROR_SUCCESS;
}

bool Registry::deleteObjectToProtect(const ProtectedObject& object)
{
    DWORD retCode = 1;

    protectedObjectIds.size();

    auto it = std::find(protectedObjectIds.begin(), protectedObjectIds.end(), std::stoi(object.id));

    if (it != protectedObjectIds.end())
    {
        protectedObjectIds.erase(it);
        retCode = RegDeleteValueA(this->m_hFilterKey, object.id.c_str());
    }

    return retCode == ERROR_SUCCESS;
}

bool Registry::addNetworkRule(const NetworkRule &rule)
{
    QJsonObject ruleJsonObject;

    //QJsonArray array = {};

    ruleJsonObject["type"]     = QString::fromUtf8(rule.type.c_str());
    ruleJsonObject["ip"]       = QString::fromUtf8(rule.ip.c_str());
    ruleJsonObject["port"]     = rule.port;
    ruleJsonObject["protocol"] = QString::fromUtf8(rule.protocol.c_str());
    ruleJsonObject["action"]   = QString::fromUtf8(rule.action.c_str());
    ruleJsonObject["offset"]   = rule.offset;

    QJsonArray qJsonArr;
    for (const uint8_t &byteValue: rule.payload) {
        qJsonArr.append(byteValue);
    }

    ruleJsonObject["payload"]  = qJsonArr;

    QString ruleJsonString(QJsonDocument{ruleJsonObject}.toJson(QJsonDocument::Compact));

    int newId = ruleIds.empty() ? 1 : *max_element(ruleIds.begin(), ruleIds.end()) + 1;

    std::string key {std::to_string(newId)};
    std::string data {ruleJsonString.toUtf8().constData()};

    DWORD retCode = RegSetValueExA(this->m_hNetworkKey, key.c_str(), 0, REG_SZ,
        reinterpret_cast<const BYTE*>(data.c_str()), static_cast<DWORD>(data.size()));

    if (retCode == ERROR_SUCCESS)
    {
        ruleIds.push_back(newId);
        return true;
    }

    return false;
}

bool Registry::deleteNetworkRule(const NetworkRule &rule)
{
    DWORD retCode = 1;

    auto it = std::find(ruleIds.begin(), ruleIds.end(), std::stoi(rule.id));

    if (it != ruleIds.end())
    {
        ruleIds.erase(it);
        retCode = RegDeleteValueA(this->m_hNetworkKey, rule.id.c_str());
    }

    return retCode == ERROR_SUCCESS;
}

std::shared_ptr<std::vector<NetworkRule>> Registry::getNetworkRules()
{
    ruleIds.clear();

    auto rules = std::make_shared<std::vector<NetworkRule>>();

    DWORD nValues;
    DWORD i, retCode;

    CHAR achValue[MAX_VALUE_NAME];
    DWORD cchValue = MAX_VALUE_NAME;

    retCode = RegQueryInfoKey(this->m_hNetworkKey, NULL, NULL, NULL, NULL, NULL, NULL, &nValues, NULL, NULL, NULL, NULL);

    if (retCode == ERROR_SUCCESS && nValues > 0)
    {
        for (i = 0, retCode = ERROR_SUCCESS; i < nValues; i++)
        {
            cchValue = MAX_VALUE_NAME;
            achValue[0] = '\0';
            retCode = RegEnumValueA(this->m_hNetworkKey, i, achValue, &cchValue, NULL, NULL, NULL, NULL);

            if (retCode == ERROR_SUCCESS)
            {
                char valueBuffer[MAX_VALUE_NAME];
                DWORD bufferSize = sizeof(valueBuffer);

                retCode = RegGetValueA(HKEY_LOCAL_MACHINE, AV_NETWORK_REG_KEY, achValue, RRF_RT_ANY, NULL, &valueBuffer, &bufferSize);

                if (retCode == ERROR_SUCCESS)
                {
                    QString ruleJsonString {QString::fromUtf8(valueBuffer)};

                    QJsonObject ruleJsonObject = QJsonDocument::fromJson(ruleJsonString.toUtf8()).object();

                    NetworkRule rule;

                    rule.id       = achValue;
                    rule.type     = ruleJsonObject["type"].toString().toUtf8().constData();
                    rule.ip       = ruleJsonObject["ip"].toString().toUtf8().constData();
                    rule.port     = ruleJsonObject["port"].toInt();
                    rule.protocol = ruleJsonObject["protocol"].toString().toUtf8().constData();
                    rule.action   = ruleJsonObject["action"].toString().toUtf8().constData();
                    //rule.payload  = ruleJsonObject["payload"].toString().toUtf8().constData();
                    //rule.offset   = ruleJsonObject["offset"].toInt();

                    rules->push_back(rule);
                    ruleIds.push_back(std::stoi(achValue));
                }
            }
        }
    }

    return rules;
}
