#ifndef REGISTRY_H
#define REGISTRY_H

#include <Windows.h>

#include "../pch.h"

struct ProtectedObject
{
    std::string id;
    std::string path;
    std::string type;
};

struct NetworkRule
{
    std::string id;
    std::string type;
    std::string ip;
    int port;
    std::string protocol;
    std::string action;
    std::vector<uint8_t> payload;
    uint16_t offset;
};

class Registry final
{
private:
    HKEY m_hRootKey;
    HKEY m_hFilterKey;
    HKEY m_hNetworkKey;

    std::vector<int> protectedObjectIds;
    std::vector<int> ruleIds;

    Registry();
    ~Registry();

    Registry(Registry const&) = delete;
    Registry& operator= (Registry const&) = delete;

public:
    static Registry* getInstance();

    //==========================================================================
    //                        Filter subsystem
    //==========================================================================

    bool addObjectToProtect(const ProtectedObject& object);
    bool deleteObjectToProtect(const ProtectedObject& object);
    std::shared_ptr<std::vector<ProtectedObject>> getObjectsToProtect();

    //==========================================================================
    //                        Network protection subsystem
    //==========================================================================

    bool addNetworkRule(const NetworkRule &rule);
    bool deleteNetworkRule(const NetworkRule &rule);
    std::shared_ptr<std::vector<NetworkRule>> getNetworkRules();
};

#endif // REGISTRY_H
