#include <QMessageBox>
#include <cstdlib>

#include "staticAnalysisRegistry.h"

#define MAX_KEY_LENGTH 255

std::unique_ptr<StaticAnalysisRegistry> StaticAnalysisRegistry::build()
{
    LSTATUS retCode;
    HKEY hKey = nullptr;
    DWORD dispozition;

    retCode = RegCreateKeyExA(
                HKEY_LOCAL_MACHINE,     // Handle of the parent registry key.
                AV_STATIC_ANAL_REG_KEY, // Name of the new key to open or create.
                0,                      // Reserved, pass 0.
                nullptr,                // The user-defined class type of this key.
                0,                      // Flags controlling the key creation
                KEY_ALL_ACCESS,         // Access level desired.
                nullptr,                // Security attributes for the key.
                &hKey,                  // Destination for the resulting handle.
                &dispozition            // Receives REG_CREATED_NEW_KEY or REG_OPENED_EXISTING_KEY.
    );
    if (retCode != ERROR_SUCCESS)
    {
        return nullptr;
    }

    if (dispozition == REG_CREATED_NEW_KEY)
    {
        /* Set ClamAVDBPath */
        retCode = RegSetValueExA(
                    hKey,
                    AV_STATIC_ANAL_CLAM_AV_DB_PATH,
                    0,
                    REG_SZ,
                    reinterpret_cast<const BYTE *>(AV_STATIC_ANAL_CLAM_AV_DB_PATH_VALUE),
                    sizeof(AV_STATIC_ANAL_CLAM_AV_DB_PATH_VALUE)
        );
        if (retCode != ERROR_SUCCESS)
        {
            return nullptr;
        }

        /* Set LevelDBPath */
        retCode = RegSetValueExA(
                    hKey,
                    AV_STATIC_ANAL_LEVEL_DB_PATH,
                    0,
                    REG_SZ,
                    reinterpret_cast<const BYTE *>(AV_STATIC_ANAL_LEVEL_DB_PATH_VALUE),
                    sizeof(AV_STATIC_ANAL_LEVEL_DB_PATH_VALUE)
        );
        if (retCode != ERROR_SUCCESS)
        {
            return nullptr;
        }

        /* Set YaraRulesMalwPath */
        retCode = RegSetValueExA(
                    hKey,
                    AV_STATIC_ANAL_YARA_RULES_MALW_PATH,
                    0,
                    REG_SZ,
                    reinterpret_cast<const BYTE *>(AV_STATIC_ANAL_YARA_RULES_MALW_PATH_VALUE),
                    sizeof(AV_STATIC_ANAL_YARA_RULES_MALW_PATH_VALUE)
        );
        if (retCode != ERROR_SUCCESS)
        {
            return nullptr;
        }

        /* Set YaraRulesPackPath */
        retCode = RegSetValueExA(
                    hKey,
                    AV_STATIC_ANAL_YARA_RULES_PACK_PATH,
                    0,
                    REG_SZ,
                    reinterpret_cast<const BYTE *>(AV_STATIC_ANAL_YARA_RULES_PACK_PATH_VALUE),
                    sizeof(AV_STATIC_ANAL_YARA_RULES_PACK_PATH_VALUE)
        );
        if (retCode != ERROR_SUCCESS)
        {
            return nullptr;
        }
    }

    return std::unique_ptr<StaticAnalysisRegistry>(new StaticAnalysisRegistry(hKey));
}

StaticAnalysisRegistry::~StaticAnalysisRegistry()
{
    CloseHandle(this->hKey);
}

bool StaticAnalysisRegistry::addObjToAnalyze(const StaticAnalysisRegistry::StaticAnalysisObject &obj)
{
    LSTATUS retCode = 0;

    DWORD freeSubkeyNum = getProbablyFreeRegistryNum();

    bool result = true;
    HKEY hSubkey = nullptr;

    /* Find free num in registry */

    std::string subkeyName = std::to_string(freeSubkeyNum);

    /* Create new key under  */
    retCode = RegCreateKeyExA(
                this->hKey,                           // Handle of the parent registry key.
                subkeyName.c_str(),                   // Name of the new key to open or create.
                0,                                    // Reserved, pass 0.
                nullptr,                              // The user-defined class type of this key.
                0,                                    // Flags controlling the key creation
                KEY_ALL_ACCESS,                       // Access level desired.
                nullptr,                              // Security attributes for the key.
                &hSubkey,                             // Destination for the resulting handle.
                nullptr                               // Receives REG_CREATED_NEW_KEY or REG_OPENED_EXISTING_KEY.
    );
    if (retCode != ERROR_SUCCESS)
    {
        QMessageBox::warning(nullptr, "StaticAnalysisRegistry::addObjToAnalyze",
                             "failed to open");
        result = false;
        goto Exit;
    }

    retCode = RegSetValueExA(
        hSubkey,
        AV_SAMPLE_FILE_PATH,
        0,
        REG_SZ,
        reinterpret_cast<const BYTE *>(obj.filePath.c_str()),
        static_cast<DWORD>(obj.filePath.length() + 1)
    );
    if (retCode != ERROR_SUCCESS)
    {
        result = false;
        goto Exit;
    }

    retCode = RegSetValueExA(
        hSubkey,
        AV_SAMPLE_CLAM_AV_STATUS,
        0,
        REG_DWORD,
        reinterpret_cast<const BYTE *>(&obj.clamAVAnalyzeStatus),
        sizeof(int)
    );
    if (retCode != ERROR_SUCCESS)
    {
        result = false;
        goto Exit;
    }

    retCode = RegSetValueExA(
        hSubkey,
        AV_SAMPLE_YARA_MALW_STATUS,
        0,
        REG_DWORD,
        reinterpret_cast<const BYTE *>(&obj.yaraMalwAnalyzeStatus),
        sizeof(int)
    );
    if (retCode != ERROR_SUCCESS)
    {
        result = false;
        goto Exit;
    }

    retCode = RegSetValueExA(
        hSubkey,
        AV_SAMPLE_YARA_PACK_STATUS,
        0,
        REG_DWORD,
        reinterpret_cast<const BYTE *>(&obj.yaraPackAnalyzeStatus),
        sizeof(int)
    );
    if (retCode != ERROR_SUCCESS)
    {
        result = false;
        goto Exit;
    }

    retCode = RegSetValueExA(
        hSubkey,
        AV_SAMPLE_SIGN_CHECK_STATUS,
        0,
        REG_DWORD,
        reinterpret_cast<const BYTE *>(&obj.signCheckStatus),
        sizeof(int)
    );
    if (retCode != ERROR_SUCCESS)
    {
        result = false;
        goto Exit;
    }

    retCode = RegSetValueExA(
        hSubkey,
        AV_SAMPLE_ML_CHECK_STATUS,
        0,
        REG_DWORD,
        reinterpret_cast<const BYTE *>(&obj.mlCheckStatus),
        sizeof(int)
    );
    if (retCode != ERROR_SUCCESS)
    {
        result = false;
        goto Exit;
    }

    result = true;

    this->usedRegistryKeyNums.insert(freeSubkeyNum);
    this->ruleNumberMap.push_back(freeSubkeyNum);

Exit:
    if (hSubkey != nullptr)
    {
        CloseHandle(hSubkey);
    }

    if (result == false)
    {
        RegDeleteKeyExA(this->hKey, subkeyName.c_str(), KEY_WOW64_64KEY, 0);
    }

    return result;
}

bool StaticAnalysisRegistry::delObjToAnalyze(uint32_t objectId)
{
    /* In GUI key id start from 0, but in registry from 1 */
    LSTATUS retCode;
    unsigned long ruleNumToDelete = this->ruleNumberMap[objectId];

    /* Update mapping object */
    for (unsigned long i = objectId; i < (this->ruleNumberMap.size() - 1); i += 1)
    {
        this->ruleNumberMap[i] = this->ruleNumberMap[i + 1];
    }
    this->ruleNumberMap.pop_back();

    /* Update registry value */
    this->usedRegistryKeyNums.erase(ruleNumToDelete);

    retCode = RegDeleteKeyExA(this->hKey, std::to_string(ruleNumToDelete).c_str(), KEY_WOW64_64KEY, 0);
    if (retCode == ERROR_SUCCESS)
    {
        return true;
    } else {
        return false;
    }
}

std::unique_ptr<std::vector<StaticAnalysisRegistry::StaticAnalysisObject> > StaticAnalysisRegistry::getAnalyzeObjects()
{
    LSTATUS retCode = 0;

    DWORD cSubKeys = getSubkeyCount(retCode);
    if (retCode != ERROR_SUCCESS)
    {
        QMessageBox::warning(nullptr, "StaticAnalysisRegistry::getAnalyzeObjects",
                             "Could not get subkey count");
        return nullptr;
    }

    std::unique_ptr<std::vector<StaticAnalysisObject> > pResultList =
        std::unique_ptr<std::vector<StaticAnalysisObject>>(
            new std::vector<StaticAnalysisObject>
    );
    char achKey[MAX_KEY_LENGTH];   // buffer for subkey name
    char achValue[MAX_PATH];
    DWORD iClamAVStatus;
    DWORD iYaraMalwStatus;
    DWORD iYaraPackStatus;
    DWORD iSignCheckStatus;
    DWORD iMLCheckStatus;
    this->usedRegistryKeyNums.clear();
    unsigned long ruleValue;

    for (DWORD i = 0; i < cSubKeys; i++)
    {
        /* Process only rules with value in name */

        /* Get subkey name */
        DWORD cbName = MAX_KEY_LENGTH;
        retCode = RegEnumKeyExA(
            this->hKey,
            i,
            reinterpret_cast<char *>(&achKey),
            &cbName,
            nullptr,
            nullptr,
            nullptr,
            nullptr
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        ruleValue = strtoul(achKey, nullptr, 10);
        if (ruleValue == 0 || ruleValue == ULONG_MAX)
        {
            continue;
        }

        /* Get filePath value */
        DWORD cchValue = MAX_PATH;
        retCode = RegGetValueA(
            this->hKey,
            achKey,
            AV_SAMPLE_FILE_PATH,
            RRF_RT_ANY,
            nullptr,
            achValue,
            &cchValue
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        /* Get ClamAVStatus value */
        DWORD intValueSize = sizeof(int);
        retCode = RegGetValueA(
            this->hKey,
            achKey,
            AV_SAMPLE_CLAM_AV_STATUS,
            RRF_RT_ANY,
            nullptr,
            reinterpret_cast<char *>(&iClamAVStatus),
            &intValueSize
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        /* Get YaraMalwStatus value */
        retCode = RegGetValueA(
            this->hKey,
            achKey,
            AV_SAMPLE_YARA_MALW_STATUS,
            RRF_RT_ANY,
            nullptr,
            reinterpret_cast<char *>(&iYaraMalwStatus),
            &intValueSize
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        /* Get YaraPackStatus value */
        retCode = RegGetValueA(
            this->hKey,
            achKey,
            AV_SAMPLE_YARA_PACK_STATUS,
            RRF_RT_ANY,
            nullptr,
            reinterpret_cast<char *>(&iYaraPackStatus),
            &intValueSize
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        /* Get SignCheckStatus value */
        retCode = RegGetValueA(
            this->hKey,
            achKey,
            AV_SAMPLE_SIGN_CHECK_STATUS,
            RRF_RT_DWORD,
            nullptr,
            reinterpret_cast<char *>(&iSignCheckStatus),
            &intValueSize
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        /* Get MLCheckStatus value */
        retCode = RegGetValueA(
            this->hKey,
            achKey,
            AV_SAMPLE_ML_CHECK_STATUS,
            RRF_RT_DWORD,
            nullptr,
            reinterpret_cast<char *>(&iMLCheckStatus),
            &intValueSize
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        pResultList->push_back(
                    StaticAnalysisObject(
                        std::string(achValue, cchValue),
                        iClamAVStatus,
                        iYaraMalwStatus,
                        iYaraPackStatus,
                        iSignCheckStatus,
                        iMLCheckStatus)
                    );
        this->usedRegistryKeyNums.insert(ruleValue);
        this->ruleNumberMap.push_back(ruleValue);
    }

    return pResultList;
}

unsigned long StaticAnalysisRegistry::getProbablyFreeRegistryNum()
{
    unsigned long result = static_cast<unsigned long>(this->usedRegistryKeyNums.size() + 1);
    if (this->usedRegistryKeyNums.find(result) == this->usedRegistryKeyNums.end())
    {
        return result;
    }

    for (unsigned long i = 0; i < this->usedRegistryKeyNums.size(); i+=1)
    {
        /* i + 1 because all registry rule nums start from 1 */
        if (this->usedRegistryKeyNums.find(i + 1) == this->usedRegistryKeyNums.end())
        {
            return i + 1;
        }
    }

    return this->ruleNumberMap[static_cast<unsigned long>(this->ruleNumberMap.size() - 1)] + 1;
}

StaticAnalysisRegistry::StaticAnalysisRegistry(HKEY inHKey)
    :hKey(inHKey)
{

}

StaticAnalysisRegistry::StaticAnalysisObject::StaticAnalysisObject(
        std::string inFilePath, DWORD inClamAVAnalyzeStatus, DWORD inYaraMalwAnalyzeStatus,
        DWORD inYaraPackAnalyzeStatus, DWORD inSignCheckStatus, DWORD inMlCheckStatus)
    :filePath(inFilePath), clamAVAnalyzeStatus(inClamAVAnalyzeStatus),
      yaraMalwAnalyzeStatus(inYaraMalwAnalyzeStatus),
      yaraPackAnalyzeStatus(inYaraPackAnalyzeStatus),
      signCheckStatus(inSignCheckStatus),
      mlCheckStatus(inMlCheckStatus)
{

}

unsigned long StaticAnalysisRegistry::getSubkeyCount(long & retCode)
{
    unsigned long cSubKeys;
    retCode = RegQueryInfoKeyA(
            this->hKey,              // key handle
            nullptr,                 // buffer for class name
            nullptr,                 // size of class string
            nullptr,                 // reserved
            &cSubKeys,               // number of subkeys
            nullptr,                 // longest subkey size
            nullptr,                 // longest class string
            nullptr,                 // number of values for this key
            nullptr,                 // longest value name
            nullptr,                 // longest value data
            nullptr,                 // security descriptor
            nullptr                  // last write time
    );
    return cSubKeys;
}
