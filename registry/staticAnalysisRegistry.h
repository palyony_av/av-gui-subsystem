#ifndef STATICANALYSISREGISTRY_H
#define STATICANALYSISREGISTRY_H

#define AV_STATIC_ANAL_REG_KEY "SOFTWARE\\PalyonyAV\\StaticAnal"
#define AV_STATIC_ANAL_VERSION "version"
#define AV_STATIC_ANAL_CLAM_AV_DB_PATH "ClamAVDBPath"
#define AV_STATIC_ANAL_LEVEL_DB_PATH "LevelDBPath"
#define AV_STATIC_ANAL_YARA_RULES_MALW_PATH "YaraRulesMalwPath"
#define AV_STATIC_ANAL_YARA_RULES_PACK_PATH "YaraRulesPackPath"

#define AV_STATIC_ANAL_VERSION_VALUE "0.0.0"
#define AV_STATIC_ANAL_CLAM_AV_DB_PATH_VALUE "DataBases\\ClamavSignatureDataBase\\maincvd\\"
#define AV_STATIC_ANAL_LEVEL_DB_PATH_VALUE "DataBases\\LevelDB\\MalwareDB\\"
#define AV_STATIC_ANAL_YARA_RULES_MALW_PATH_VALUE "Rules\\YaraRules\\ForMalware"
#define AV_STATIC_ANAL_YARA_RULES_PACK_PATH_VALUE "Rules\\YaraRules\\ForPackers"

#define AV_SAMPLE_FILE_PATH "filePath"
#define AV_SAMPLE_CLAM_AV_STATUS "ClamAVStatus"
#define AV_SAMPLE_YARA_MALW_STATUS "YaraMalwStatus"
#define AV_SAMPLE_YARA_PACK_STATUS "YaraPackStatus"
#define AV_SAMPLE_SIGN_CHECK_STATUS "SignCheckStatus"
#define AV_SAMPLE_ML_CHECK_STATUS "MLCheckStatus"

#include <Windows.h>
#include <memory>
#include <string>
#include <vector>
#include <set>

class StaticAnalysisRegistry
{
public:
    struct StaticAnalysisObject;

    static std::unique_ptr<StaticAnalysisRegistry> build();
    ~StaticAnalysisRegistry();

    bool addObjToAnalyze(const StaticAnalysisObject& obj);
    bool delObjToAnalyze(uint32_t objectId);
    std::unique_ptr<std::vector<StaticAnalysisObject>> getAnalyzeObjects();

private:
    HKEY hKey;
    std::set<unsigned long> usedRegistryKeyNums;
    /* Do not use std::map because the values are not sparse */
    std::vector<unsigned long> ruleNumberMap;

    unsigned long getProbablyFreeRegistryNum();
    unsigned long getSubkeyCount(long & retCode);
    StaticAnalysisRegistry(HKEY inHKey);
};

struct StaticAnalysisRegistry::StaticAnalysisObject
{
    StaticAnalysisObject(std::string inFilePath, DWORD inClamAVAnalyzeStatus,
                         DWORD inYaraMalwAnalyzeStatus, DWORD inYaraPackAnalyzeStatus,
                         DWORD inSignCheckStatus, DWORD inMlCheckStatus);
    ~StaticAnalysisObject() = default;

    std::string filePath;
    DWORD clamAVAnalyzeStatus;
    DWORD yaraMalwAnalyzeStatus;
    DWORD yaraPackAnalyzeStatus;
    DWORD signCheckStatus;
    DWORD mlCheckStatus;
};

#endif // STATICANALYSISREGISTRY_H
