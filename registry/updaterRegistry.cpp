#include <QMessageBox>

#include "updaterRegistry.h"

#define MAX_KEY_LENGTH 255

std::unique_ptr<UpdaterRegistry> UpdaterRegistry::build()
{
    LSTATUS retCode;
    HKEY hKey = nullptr;
    DWORD dispozition;

    retCode = RegCreateKeyExA(
                HKEY_LOCAL_MACHINE,     // Handle of the parent registry key.
                AV_UPDATER_REG_KEY,     // Name of the new key to open or create.
                0,                      // Reserved, pass 0.
                nullptr,                // The user-defined class type of this key.
                0,                      // Flags controlling the key creation
                KEY_ALL_ACCESS,         // Access level desired.
                nullptr,                // Security attributes for the key.
                &hKey,                  // Destination for the resulting handle.
                &dispozition            // Receives REG_CREATED_NEW_KEY or REG_OPENED_EXISTING_KEY.
    );
    if (retCode != ERROR_SUCCESS)
    {
        return nullptr;
    }

    if (dispozition == REG_CREATED_NEW_KEY)
    {
        /* Set ip address of the remote server */
        retCode = RegSetValueExA(
                    hKey,
                    AV_UPDATER_NETADDR,
                    0,
                    REG_SZ,
                    reinterpret_cast<const BYTE *>(AV_UPDATER_NETADDR_VALUE),
                    sizeof(AV_UPDATER_NETADDR_VALUE)
        );
        if (retCode != ERROR_SUCCESS)
        {
            return nullptr;
        }
    }

    return std::unique_ptr<UpdaterRegistry>(new UpdaterRegistry(hKey));
}

UpdaterRegistry::~UpdaterRegistry()
{
    CloseHandle(this->hKey);
}

void UpdaterRegistry::createUpdateInfo(const char * szModule, const char * szModulePath)
{
    LSTATUS retCode;
    HKEY hSubKey = nullptr;
    DWORD dispozition;

    retCode = RegCreateKeyExA(
                this->hKey,             // Handle of the parent registry key.
                szModule,               // Name of the new key to open or create.
                0,                      // Reserved, pass 0.
                nullptr,                // The user-defined class type of this key.
                0,                      // Flags controlling the key creation
                KEY_ALL_ACCESS,         // Access level desired.
                nullptr,                // Security attributes for the key.
                &hSubKey,               // Destination for the resulting handle.
                &dispozition            // Receives REG_CREATED_NEW_KEY or REG_OPENED_EXISTING_KEY.
    );
    if (retCode != ERROR_SUCCESS)
    {
        return;
    }

    if (dispozition == REG_CREATED_NEW_KEY)
    {
        /* Set module path if it does not exist */
        retCode = RegSetValueExA(
                    hSubKey,
                    AV_UPDATER_MODULE_PATH,
                    0,
                    REG_SZ,
                    reinterpret_cast<const BYTE *>(szModulePath),
                    static_cast<DWORD>(std::strlen(szModulePath)) + 1   // and null character
        );
        if (retCode != ERROR_SUCCESS)
        {
            return;
        }

        /* Set default module version if it does not exist */
        retCode = RegSetValueExA(
                    hSubKey,
                    AV_UPDATER_MODULE_VERSION,
                    0,
                    REG_SZ,
                    reinterpret_cast<const BYTE *>(AV_UPDATER_MODULE_VERSION_VALUE),
                    sizeof(AV_UPDATER_MODULE_VERSION_VALUE)
        );
        if (retCode != ERROR_SUCCESS)
        {
            return;
        }

        DWORD moduleState = AV_UPDATER_MODULE_STATE_VALUE;
        /* Set default module state if it does not exist */
        retCode = RegSetValueExA(
                    hSubKey,
                    AV_UPDATER_MODULE_STATE,
                    0,
                    REG_DWORD,
                    reinterpret_cast<const BYTE *>(&moduleState),
                    sizeof(DWORD)
        );
        if (retCode != ERROR_SUCCESS)
        {
            return;
        }
    }
}

std::unique_ptr<std::vector<UpdaterRegistry::UpdaterObject> > UpdaterRegistry::getAnalyzeObjects()
{
    LSTATUS retCode = 0;

    DWORD cSubKeys = getSubkeyCount(retCode);
    if (retCode != ERROR_SUCCESS)
    {
        QMessageBox::warning(nullptr, "UpdaterRegistry::getAnalyzeObjects",
                             "Could not get subkey count");
        return nullptr;
    }

    std::unique_ptr<std::vector<UpdaterObject> > pResultList =
        std::unique_ptr<std::vector<UpdaterObject>>(
            new std::vector<UpdaterObject>
    );
    char achKey[MAX_KEY_LENGTH];   // buffer for subkey name
    char modulePath[MAX_PATH];
    char version[MAX_KEY_LENGTH];

    /* because of usage forward_list the */
    for (DWORD i = 0; i < cSubKeys; i++)
    {
        UpdaterRegistry::AVModuleState state;
        DWORD stateSize;

        /* Get subkey name */
        DWORD cbName = MAX_KEY_LENGTH;
        retCode = RegEnumKeyExA(
            this->hKey,
            i,
            reinterpret_cast<char *>(&achKey),
            &cbName,
            nullptr,
            nullptr,
            nullptr,
            nullptr
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        /* Get modulePath value */
        DWORD modulePathSize = MAX_PATH;
        retCode = RegGetValueA(
            this->hKey,
            achKey,
            AV_UPDATER_MODULE_PATH,
            RRF_RT_REG_SZ,
            nullptr,
            modulePath,
            &modulePathSize
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        /* Get version value */
        DWORD versionSize = MAX_KEY_LENGTH;
        retCode = RegGetValueA(
            this->hKey,
            achKey,
            AV_UPDATER_MODULE_VERSION,
            RRF_RT_REG_SZ,
            nullptr,
            version,
            &versionSize
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        /* Get state value */
        retCode = RegGetValueA(
            this->hKey,
            achKey,
            AV_UPDATER_MODULE_STATE,
            RRF_RT_DWORD,
            nullptr,
            reinterpret_cast<char *>(&state),
            &stateSize
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        pResultList->push_back(
                    UpdaterRegistry::UpdaterObject(
                        std::string(achKey, cbName),
                        std::string(modulePath, modulePathSize - 1),
                        std::string(version, versionSize - 1),
                        state));
    }

    return pResultList;
}

void UpdaterRegistry::setDownloadedToUpdated()
{
    LSTATUS retCode = 0;

    DWORD cSubKeys = getSubkeyCount(retCode);
    if (retCode != ERROR_SUCCESS)
    {
        QMessageBox::warning(nullptr, "UpdaterRegistry::getAnalyzeObjects",
                             "Could not get subkey count");
        return;
    }

    std::unique_ptr<std::vector<UpdaterObject> > pResultList =
        std::unique_ptr<std::vector<UpdaterObject>>(
            new std::vector<UpdaterObject>
    );
    char achKey[MAX_KEY_LENGTH];   // buffer for subkey name

    /* because of usage forward_list the */
    for (DWORD i = 0; i < cSubKeys; i++)
    {
        UpdaterRegistry::AVModuleState state;
        DWORD stateSize = sizeof(AVModuleState);

        /* Get subkey name */
        DWORD cbName = MAX_KEY_LENGTH;
        retCode = RegEnumKeyExA(
            this->hKey,
            i,
            reinterpret_cast<char *>(&achKey),
            &cbName,
            nullptr,
            nullptr,
            nullptr,
            nullptr
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        /* Get state value */
        retCode = RegGetValueA(
            this->hKey,
            achKey,
            AV_UPDATER_MODULE_STATE,
            RRF_RT_DWORD,
            nullptr,
            reinterpret_cast<char *>(&state),
            &stateSize
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        if (state != AVModuleState::downloaded)
        {
            continue;
        }

        HKEY hSubKey = nullptr;
        retCode = RegCreateKeyExA(
                    this->hKey,
                    achKey,
                    0,
                    nullptr,
                    0,
                    KEY_ALL_ACCESS,
                    nullptr,
                    &hSubKey,
                    nullptr
        );
        if (retCode != ERROR_SUCCESS)
        {
            continue;
        }

        state = AVModuleState::updated;
        retCode = RegSetValueExA(
                    hSubKey,
                    AV_UPDATER_MODULE_STATE,
                    0,
                    REG_DWORD,
                    reinterpret_cast<const BYTE *>(&state),
                    sizeof(AVModuleState)
        );
        CloseHandle(hSubKey);
    }
}

UpdaterRegistry::UpdaterRegistry(HKEY inHKey)
    :hKey(inHKey)
{
    this->createUpdateInfo(AV_UPDATER_NETWORK_MODULE, AV_UPDATER_NETWORK_MODULE_PATH);
    this->createUpdateInfo(AV_UPDATER_STATIC_MODULE, AV_UPDATER_STATIC_MODULE_PATH);
    this->createUpdateInfo(AV_UPDATER_GUI_MODULE, AV_UPDATER_GUI_MODULE_PATH);
}

UpdaterRegistry::UpdaterObject::UpdaterObject(std::string inModule, std::string inPath, std::string inVersion, UpdaterRegistry::AVModuleState inState)
    :path(inPath), version(inVersion), module(inModule), state(inState)
{

}

unsigned long UpdaterRegistry::getSubkeyCount(long & retCode)
{
    unsigned long cSubKeys;
    retCode = RegQueryInfoKeyA(
            this->hKey,              // key handle
            nullptr,                 // buffer for class name
            nullptr,                 // size of class string
            nullptr,                 // reserved
            &cSubKeys,               // number of subkeys
            nullptr,                 // longest subkey size
            nullptr,                 // longest class string
            nullptr,                 // number of values for this key
            nullptr,                 // longest value name
            nullptr,                 // longest value data
            nullptr,                 // security descriptor
            nullptr                  // last write time
    );
    return cSubKeys;
}
