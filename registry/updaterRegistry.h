#ifndef UPDATERREGISTRY_H
#define UPDATERREGISTRY_H

#include <Windows.h>
#include <memory>
#include <vector>
#include <string>

#define AV_UPDATER_REG_KEY "SOFTWARE\\PalyonyAV\\Updater"
#define AV_UPDATER_NETADDR "netaddr"
#define AV_UPDATER_NETADDR_VALUE "http://127.0.0.1:8030"

#define AV_UPDATER_MODULE_VERSION "version"
#define AV_UPDATER_MODULE_PATH "path"
#define AV_UPDATER_MODULE_STATE "state"

#define AV_UPDATER_MODULE_VERSION_VALUE "0.0.0"
#define AV_UPDATER_MODULE_STATE_VALUE 0

/* Used for test purpose only */
#define AV_UPDATER_NETWORK_MODULE "network"
#define AV_UPDATER_STATIC_MODULE "static"
#define AV_UPDATER_GUI_MODULE "gui"

#define AV_UPDATER_NETWORK_MODULE_PATH "lib\\net-filter-dll.dll"
#define AV_UPDATER_STATIC_MODULE_PATH "lib\\Static_Analysis_Subsystem.dll"
#define AV_UPDATER_GUI_MODULE_PATH "AVGUISubsystem.exe"


class UpdaterRegistry
{
public:
    enum class AVModuleState: DWORD
    {
        updated = 0,
        downloaded = 1,
        waitForDownloading = 2
    };

    struct UpdaterObject;

    static std::unique_ptr<UpdaterRegistry> build();
    ~UpdaterRegistry();

    std::unique_ptr<std::vector<UpdaterObject>> getAnalyzeObjects();
    void setDownloadedToUpdated();

private:
    HKEY hKey;
    void createUpdateInfo(const char * szModule, const char * szModulePath);
    UpdaterRegistry(HKEY inHKey);
    unsigned long getSubkeyCount(long & retCode);
};

struct UpdaterRegistry::UpdaterObject
{
    UpdaterObject(std::string module, std::string inPath, std::string inVersion, UpdaterRegistry::AVModuleState inState);
    ~UpdaterObject() = default;

    std::string path;
    std::string version;
    std::string module;
    UpdaterRegistry::AVModuleState state;
};

#endif // UPDATERREGISTRY_H
