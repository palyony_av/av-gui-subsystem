#include <QMessageBox>
#include <QFileDialog>
#include <cassert>

#include "cloudScanTabPage.h"
#include "ui_cloudScanTabPage.h"

CloudScanTabPage::CloudScanTabPage(QWidget *parent) :
    TabWidget(parent),
    ui(new Ui::CloudScanTabPage)
{
    ui->setupUi(this);

    ui->cloudScanResultTable->setColumnCount(3);
    ui->cloudScanResultTable->setHorizontalHeaderLabels({"Path", "ProcessingStatus", "Answer"});
    ui->cloudScanResultTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);

    this->pCloudScanRegistry = CloudScanRegistry::build();
    if (this->pCloudScanRegistry == nullptr)
    {
        QMessageBox::warning(nullptr, "CloudScanTabPage::CloudScanTabPage",
                             "Could not open HKLM\\" AV_CLOUD_SCAN_REG_KEY " registry path");
    }

    this->pCloudScanModule = AVDllModule::buildModule(DLL_CLOUD_SCAN_MODULE_PATH);
    if (this->pCloudScanModule == nullptr)
    {
        QMessageBox::warning(nullptr, "CloudScanTabPage::CloudScanTabPage",
                             "The " DLL_CLOUD_SCAN_MODULE_PATH " did not found");
    }

    this->refresh();
}

CloudScanTabPage::~CloudScanTabPage()
{
    delete ui;
}

void CloudScanTabPage::on_scanFileButton_clicked()
{
    if (this->pCloudScanRegistry == nullptr)
    {
        QMessageBox::warning(nullptr, "CloudScanTabPage::on_scanFileButton_clicked",
                             "Could not open registry key for sandbox analyzer");
        return;
    }

    if (this->pCloudScanModule == nullptr)
    {
        QMessageBox::warning(nullptr, "CloudScanTabPage::on_scanFileButton_clicked",
                             "The " DLL_CLOUD_SCAN_MODULE_PATH " did not found");
        return;
    }

    QString filename = QFileDialog::getOpenFileName(this,
                                                    "Choose file to analyze",
                                                    QString(),
                                                    tr("Executable (*.exe *.msi *.bat)"));
    if (filename.size() == 0)
    {
        return;
    }

    CloudScanObject obj(filename.toUtf8().constData(), CloudScanAnalyzerState::waitForAnalysis, 0);
    if (!this->pCloudScanRegistry->addObjToAnalyze(obj))
    {
        return;
    }

    int rowNum = this->ui->cloudScanResultTable->rowCount();

    this->ui->cloudScanResultTable->insertRow(rowNum);

    this->ui->cloudScanResultTable->setItem(rowNum, 0, new QTableWidgetItem(filename));
    this->ui->cloudScanResultTable->setItem(rowNum, 1, new QTableWidgetItem(AV_CLOUD_SCAN_STATE_WAIT));
    this->ui->cloudScanResultTable->setItem(rowNum, 2, new QTableWidgetItem("Undefined"));

    this->pCloudScanModule->updateModuleData();
}

void CloudScanTabPage::on_refreshButton_clicked()
{
    this->refresh();
}

void CloudScanTabPage::refresh()
{
    if (this->pCloudScanRegistry == nullptr)
    {
        QMessageBox::warning(nullptr, "CloudScanTabPage::refresh",
                             "Could not open registry key for sandbox analyzer");
        return;
    }

    std::unique_ptr<std::forward_list<CloudScanObject>> objects = this->pCloudScanRegistry->getAnalyzeObjects();
    this->ui->cloudScanResultTable->setRowCount(0);
    for (const CloudScanObject & obj : *objects)
    {
        int rowNum = this->ui->cloudScanResultTable->rowCount();

        this->ui->cloudScanResultTable->insertRow(rowNum);

        this->ui->cloudScanResultTable->setItem(rowNum, 0, new QTableWidgetItem(QString::fromStdString(obj.filePath)));
        switch (obj.state) {
        case CloudScanAnalyzerState::waitForAnalysis:
            this->ui->cloudScanResultTable->setItem(rowNum, 1, new QTableWidgetItem(AV_CLOUD_SCAN_STATE_WAIT));
            break;
        case CloudScanAnalyzerState::analyseIsRunning:
            this->ui->cloudScanResultTable->setItem(rowNum, 1, new QTableWidgetItem(AV_CLOUD_SCAN_STATE_RUNNING));
            break;
        case CloudScanAnalyzerState::analyseIsEnded:
            this->ui->cloudScanResultTable->setItem(rowNum, 1, new QTableWidgetItem(AV_CLOUD_SCAN_STATE_ENDED));
            break;
        case CloudScanAnalyzerState::analyseIsFailed:
            this->ui->cloudScanResultTable->setItem(rowNum, 1, new QTableWidgetItem(AV_CLOUD_SCAN_STATE_FAILED));
            break;
        default:
            assert(((void)"Unhandled special enum constant!", 0));
            break;
        }

        switch (obj.answer) {
        case 0:
            this->ui->cloudScanResultTable->setItem(rowNum, 2, new QTableWidgetItem(AV_CLOUD_SCAN_ANSWER_BENIGN));
            break;
        case 1:
            this->ui->cloudScanResultTable->setItem(rowNum, 2, new QTableWidgetItem(AV_CLOUD_SCAN_ANSWER_MALWARE));
            break;
        default:
            this->ui->cloudScanResultTable->setItem(rowNum, 2, new QTableWidgetItem(AV_CLOUD_SCAN_ANSWER_UNDEFINED));
            break;
        }
    }
}

void CloudScanTabPage::on_switchButton_clicked()
{
    ui->statusLabel->setText("Process...");
    ui->statusLabel->setStyleSheet(QString("QLabel { color : %0; }").arg("orange"));

    if (this->pCloudScanModule == nullptr)
    {
        QMessageBox::warning(nullptr, "CloudScanTabPage::on_switchButton_clicked",
                             "The " DLL_CLOUD_SCAN_MODULE_NAME " did not found");
        goto Exit;
    }

    if (this->bCloudScanModuleEnabled)
    {
        if(!this->pCloudScanModule->uninstallModule())
        {
            goto Exit;
        }

        this->bCloudScanModuleEnabled = false;
    } else {
        if (!this->pCloudScanModule->installModule())
        {
            goto Exit;
        }

        this->bCloudScanModuleEnabled = true;
    }

Exit:
    ui->statusLabel->setText(this->bCloudScanModuleEnabled ? "ON" : "OFF");
    ui->statusLabel->setStyleSheet(QString("QLabel { color : %0; }").arg(this->bCloudScanModuleEnabled ? "green" : "red"));
}
