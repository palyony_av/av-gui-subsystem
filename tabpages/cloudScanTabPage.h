#ifndef CLOUDSCANTABPAGE_H
#define CLOUDSCANTABPAGE_H

#include "tabwidget.h"
#include "registry/cloudScanRegistry.h"
#include "module/avDllModule.h"

#define AV_CLOUD_SCAN_STATE_WAIT "Wait for analysis"
#define AV_CLOUD_SCAN_STATE_RUNNING "Analyse is running"
#define AV_CLOUD_SCAN_STATE_ENDED "Analyse is ended"
#define AV_CLOUD_SCAN_STATE_FAILED "Analyse is failed"

#define AV_CLOUD_SCAN_ANSWER_BENIGN "Benign"
#define AV_CLOUD_SCAN_ANSWER_MALWARE "Malware"
#define AV_CLOUD_SCAN_ANSWER_UNDEFINED "Undefined"

namespace Ui {
class CloudScanTabPage;
}

class CloudScanTabPage : public TabWidget
{
    Q_OBJECT

public:
    explicit CloudScanTabPage(QWidget *parent = nullptr);
    ~CloudScanTabPage();

private slots:
    void on_scanFileButton_clicked();
    void on_refreshButton_clicked();
    void on_switchButton_clicked();

private:
    void refresh();

    bool bCloudScanModuleEnabled = false;
    Ui::CloudScanTabPage *ui;
    std::unique_ptr<CloudScanRegistry> pCloudScanRegistry;
    std::unique_ptr<AVDllModule> pCloudScanModule;
};

#endif // CLOUDSCANTABPAGE_H
