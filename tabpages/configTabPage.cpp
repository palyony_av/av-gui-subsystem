#include <QMessageBox>
#include <QFileDialog>

#include "configTabPage.h"
#include "ui_configTabPage.h"

ConfigTabPage::ConfigTabPage(QWidget *parent) :
    TabWidget(parent),
    ui(new Ui::ConfigTabPage)
{
    ui->setupUi(this);

    this->pConfigRegistry = ConfigRegistry::build();
    if (this->pConfigRegistry == nullptr)
    {
        QMessageBox::warning(nullptr, "CloudScanTabPage::CloudScanTabPage",
                             "Could not open registry keys for config");
    }

    this->ui->leCloudScanServerIP->setValidator(new QRegExpValidator( QRegExp("([0-9]{1,3}[\\.]){3}[0-9]{1,3}") ));
    this->ui->leCloudScanServerPort->setValidator(new QRegExpValidator( QRegExp("[0-9]{0,5}") ));

    this->ui->leUpdateServerIp->setValidator(new QRegExpValidator( QRegExp("([0-9]{1,3}[\\.]){3}[0-9]{1,3}") ));
    this->ui->leUpdateServerPort->setValidator(new QRegExpValidator( QRegExp("[0-9]{0,5}") ));

    this->refresh();
}

ConfigTabPage::~ConfigTabPage()
{
    delete ui;
}

void ConfigTabPage::refresh()
{
    if (this->pConfigRegistry == nullptr)
    {
        QMessageBox::warning(nullptr, "ConfigTabPage::refresh",
                             "Could not open registry key for config");
        return;
    }

    auto resultCloudScan = this->pConfigRegistry->getCloudScanNetAddr();
    if (resultCloudScan.first.size() == 0)
    {
        QMessageBox::warning(nullptr, "ConfigTabPage::refresh",
                             "Could not get correct registry parameters");
        return;
    }

    this->ui->leCloudScanServerIP->setText(QString::fromStdString(resultCloudScan.first));
    this->ui->leCloudScanServerPort->setText(QString::fromStdString(resultCloudScan.second));

    auto resultUpdate = this->pConfigRegistry->getUpdateNetAddr();
    if (resultUpdate.first.size() == 0)
    {
        QMessageBox::warning(nullptr, "ConfigTabPage::refresh",
                             "Could not get correct registry parameters");
        return;
    }

    this->ui->leUpdateServerIp->setText(QString::fromStdString(resultUpdate.first));
    this->ui->leUpdateServerPort->setText(QString::fromStdString(resultUpdate.second));

    std::string clamAVDBPath;
    std::string levelDBPath;
    std::string yaraRulesMalwPath;
    std::string yaraRulesPackPath;
    std::tie(clamAVDBPath, levelDBPath, yaraRulesMalwPath, yaraRulesPackPath) = this->pConfigRegistry->getStaticAnalysisPath();
    if (clamAVDBPath.size() == 0)
    {
        QMessageBox::warning(nullptr, "ConfigTabPage::refresh",
                             "Could not get correct registry parameters");
        return;
    }

    this->ui->lClamAVDBPathValue->setText(QString::fromStdString(clamAVDBPath));
    this->ui->lLevelDBPathValue->setText(QString::fromStdString(levelDBPath));
    this->ui->lYaraRulesMalwPathValue->setText(QString::fromStdString(yaraRulesMalwPath));
    this->ui->lYaraRulesPackPathValue->setText(QString::fromStdString(yaraRulesPackPath));
}

void ConfigTabPage::on_cbCloudScanServer_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked)
    {
        this->ui->lCloudScanServerIp->setEnabled(true);
        this->ui->lCloudScanServerPort->setEnabled(true);
        this->ui->leCloudScanServerIP->setEnabled(true);
        this->ui->leCloudScanServerPort->setEnabled(true);
        this->ui->pbSave->setEnabled(true);
    } else if (arg1 == Qt::Unchecked)
    {
        this->ui->lCloudScanServerIp->setEnabled(false);
        this->ui->lCloudScanServerPort->setEnabled(true);
        this->ui->leCloudScanServerIP->setEnabled(false);
        this->ui->leCloudScanServerPort->setEnabled(false);

        if (this->ui->cbUpdateServer->checkState() == Qt::Unchecked &&
                this->ui->cbStaticAnalysisFiles->checkState() == Qt::Unchecked)
        {
            this->ui->pbSave->setEnabled(false);
        }
    }
}

void ConfigTabPage::on_pbSave_clicked()
{
    if (this->ui->cbCloudScanServer->checkState() == Qt::Checked)
    {
        std::string ipAddr = this->ui->leCloudScanServerIP->text().toStdString();
        std::string port = this->ui->leCloudScanServerPort->text().toStdString();
        this->pConfigRegistry->setCloudScanNetAddr(ipAddr, port);
    }

    if (this->ui->cbUpdateServer->checkState() == Qt::Checked)
    {
        std::string ipAddr = this->ui->leUpdateServerIp->text().toStdString();
        std::string port = this->ui->leUpdateServerPort->text().toStdString();
        this->pConfigRegistry->setUpdateNetAddr(ipAddr, port);
    }

    if (this->ui->cbStaticAnalysisFiles->checkState() == Qt::Checked)
    {
        std::string clamAVDBPath = this->ui->lClamAVDBPathValue->text().toStdString();
        std::string levelDBPath = this->ui->lLevelDBPathValue->text().toStdString();
        std::string yaraRulesMalwPath = this->ui->lYaraRulesMalwPathValue->text().toStdString();
        std::string yaraRulesPackPath = this->ui->lYaraRulesPackPathValue->text().toStdString();
        this->pConfigRegistry->setStaticAnalysisPath(clamAVDBPath, levelDBPath,
                                                     yaraRulesMalwPath, yaraRulesPackPath);
    }
}

void ConfigTabPage::on_cbUpdateServer_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked)
    {
        this->ui->lUpdateServerIp->setEnabled(true);
        this->ui->lUpdateServerPort->setEnabled(true);
        this->ui->leUpdateServerIp->setEnabled(true);
        this->ui->leUpdateServerPort->setEnabled(true);
        this->ui->pbSave->setEnabled(true);
    } else if (arg1 == Qt::Unchecked)
    {
        this->ui->lUpdateServerIp->setEnabled(false);
        this->ui->lUpdateServerPort->setEnabled(false);
        this->ui->leUpdateServerIp->setEnabled(false);
        this->ui->leUpdateServerPort->setEnabled(false);

        if (this->ui->cbCloudScanServer->checkState() == Qt::Unchecked &&
                this->ui->cbStaticAnalysisFiles->checkState() == Qt::Unchecked)
        {
            this->ui->pbSave->setEnabled(false);
        }
    }
}

void ConfigTabPage::on_cbStaticAnalysisFiles_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked)
    {
        this->ui->lClamAVDBPathCaption->setEnabled(true);
        this->ui->pbChangeClamAVDBPath->setEnabled(true);
        this->ui->lClamAVDBPathValue->setEnabled(true);

        this->ui->lLevelDBPathCaption->setEnabled(true);
        this->ui->pbChangeLevelDBPath->setEnabled(true);
        this->ui->lLevelDBPathValue->setEnabled(true);

        this->ui->lYaraRulesMalwPathCaption->setEnabled(true);
        this->ui->pbChangeYaraRulesMalwPath->setEnabled(true);
        this->ui->lYaraRulesMalwPathValue->setEnabled(true);

        this->ui->lYaraRulesPackPathCaption->setEnabled(true);
        this->ui->pbChangeYaraRulesPackPath->setEnabled(true);
        this->ui->lYaraRulesPackPathValue->setEnabled(true);
    } else if (arg1 == Qt::Unchecked)
    {
        this->ui->lClamAVDBPathCaption->setEnabled(false);
        this->ui->pbChangeClamAVDBPath->setEnabled(false);
        this->ui->lClamAVDBPathValue->setEnabled(false);

        this->ui->lLevelDBPathCaption->setEnabled(false);
        this->ui->pbChangeLevelDBPath->setEnabled(false);
        this->ui->lLevelDBPathValue->setEnabled(false);

        this->ui->lYaraRulesMalwPathCaption->setEnabled(false);
        this->ui->pbChangeYaraRulesMalwPath->setEnabled(false);
        this->ui->lYaraRulesMalwPathValue->setEnabled(false);

        this->ui->lYaraRulesPackPathCaption->setEnabled(false);
        this->ui->pbChangeYaraRulesPackPath->setEnabled(false);
        this->ui->lYaraRulesPackPathValue->setEnabled(false);

        if (this->ui->cbCloudScanServer->checkState() == Qt::Unchecked &&
                this->ui->cbUpdateServer->checkState() == Qt::Unchecked)
        {
            this->ui->pbSave->setEnabled(false);
        }
    }
}

void ConfigTabPage::on_pbChangeClamAVDBPath_clicked()
{
    QString directory = QFileDialog::getExistingDirectory(this, tr("Choose catalog"));

    if (directory.size() == 0)
    {
        return;
    }

    this->ui->lClamAVDBPathValue->setText(directory);
}

void ConfigTabPage::on_pbChangeLevelDBPath_clicked()
{
    QString directory = QFileDialog::getExistingDirectory(this, tr("Choose catalog"));

    if (directory.size() == 0)
    {
        return;
    }

    this->ui->lLevelDBPathValue->setText(directory);
}

void ConfigTabPage::on_pbChangeYaraRulesMalwPath_clicked()
{
    QString directory = QFileDialog::getExistingDirectory(this, tr("Choose catalog"));

    if (directory.size() == 0)
    {
        return;
    }

    this->ui->lYaraRulesMalwPathValue->setText(directory);
}

void ConfigTabPage::on_pbChangeYaraRulesPackPath_clicked()
{
    QString directory = QFileDialog::getExistingDirectory(this, tr("Choose catalog"));

    if (directory.size() == 0)
    {
        return;
    }

    this->ui->lYaraRulesPackPathValue->setText(directory);
}
