#ifndef CONFIGTABPAGE_H
#define CONFIGTABPAGE_H

#include <tabwidget.h>
#include "registry/configRegistry.h"

namespace Ui {
class ConfigTabPage;
}

class ConfigTabPage : public TabWidget
{
    Q_OBJECT

public:
    explicit ConfigTabPage(QWidget *parent = nullptr);
    ~ConfigTabPage();
    void refresh();

private slots:
    void on_cbCloudScanServer_stateChanged(int arg1);

    void on_pbSave_clicked();

    void on_cbUpdateServer_stateChanged(int arg1);

    void on_cbStaticAnalysisFiles_stateChanged(int arg1);

    void on_pbChangeClamAVDBPath_clicked();

    void on_pbChangeLevelDBPath_clicked();

    void on_pbChangeYaraRulesMalwPath_clicked();

    void on_pbChangeYaraRulesPackPath_clicked();

private:
    Ui::ConfigTabPage *ui;
    std::unique_ptr<ConfigRegistry> pConfigRegistry;
};

#endif // CONFIGTABPAGE_H
