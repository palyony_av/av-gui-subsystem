#include "filtersystemtabpage.h"
#include "ui_filtersystemtabpage.h"
#include "../dialogs/filtersystemdialog.h"

#include <QMessageBox>
#include <QErrorMessage>

#include "QStandardItemModel"
#include "QStandardItem"

FilterSystemTabPage::FilterSystemTabPage(QWidget *parent)
    : TabWidget(parent),
      ui(new Ui::FilterSystemTabPage),
      reg(nullptr)
{
    ui->setupUi(this);

    ui->protectedObjectsTableWidget->setColumnCount(3);
    ui->protectedObjectsTableWidget->setHorizontalHeaderLabels(QStringList() << "ID" << "Type" << "Path");
    ui->protectedObjectsTableWidget->horizontalHeader()->setStretchLastSection(true);
    ui->protectedObjectsTableWidget->verticalHeader()->hide();
    ui->protectedObjectsTableWidget->horizontalHeader()->hideSection(0);

    try
    {
        reg = Registry::getInstance();
    }
    catch (const std::exception& e)
    {
        QMessageBox::critical(nullptr, "AV::FilterSystemTabPage", e.what(), QMessageBox::Ok, 0, 0);
        exit(1);
    }

    this->filterSubsystemModule = AVDllModule::buildModule(DLL_FILTER_MODULE_PATH);
    if (this->filterSubsystemModule == nullptr)
    {
        QMessageBox::warning(nullptr, "AV::FilterSystemTabPage", "The " DLL_FILTER_MODULE_PATH " did not found");
    }

    refresh();
}

FilterSystemTabPage::~FilterSystemTabPage()
{
    delete ui;
}

void FilterSystemTabPage::refresh()
{
    ui->protectedObjectsTableWidget->setRowCount(0);

    auto protectedObjects = reg->getObjectsToProtect();

    for (auto it = protectedObjects->begin(); it != protectedObjects->end(); it++)
    {
        QString id   = QString::fromUtf8(it->id.c_str());
        QString name = QString::fromUtf8(it->path.c_str());
        QString type = QString::fromUtf8(it->type.c_str());

        int row = ui->protectedObjectsTableWidget->rowCount();

        ui->protectedObjectsTableWidget->insertRow(row);

        ui->protectedObjectsTableWidget->setItem(row, 0, new QTableWidgetItem(id));
        ui->protectedObjectsTableWidget->setItem(row, 1, new QTableWidgetItem(type));
        ui->protectedObjectsTableWidget->setItem(row, 2, new QTableWidgetItem(name));
    }
}

void FilterSystemTabPage::on_switchPushButton_clicked()
{
    ui->statusLabel->setText("Process...");
    ui->statusLabel->setStyleSheet(QString("QLabel { color : %0; }").arg("orange"));

    if (status)
    {
        if (this->filterSubsystemModule->uninstallModule())
        {
            status = false;
        }
        else
        {
            QMessageBox::critical(nullptr, "Error", "Failed to uninstall module", QMessageBox::Ok, 0, 0);
        }
    }
    else
    {
        if (this->filterSubsystemModule->installModule())
        {
            status = true;

            if (!this->filterSubsystemModule->updateModuleData())
            {
                QMessageBox::critical(nullptr, "Error", "Failed to update module data", QMessageBox::Ok, 0, 0);
            }
        }
        else
        {
            QMessageBox::critical(nullptr, "Error", "Failed to install module", QMessageBox::Ok, 0, 0);
        }
    }

    ui->statusLabel->setText(status ? "ON" : "OFF");
    ui->statusLabel->setStyleSheet(QString("QLabel { color : %0; }").arg(status ? "green" : "red"));
}

void FilterSystemTabPage::on_refreshPushButton_clicked()
{
    refresh();
}

void FilterSystemTabPage::on_addPushButton_clicked()
{
    FilterSystemDialog dialog;

    if (dialog.exec() == QDialog::Accepted)
    {
        ProtectedObject object = dialog.getData();

        // Insert data to registry
        if (!reg->addObjectToProtect(object))
        {
            QString title("Error - Registry");
            QString message("Failed to add object to registry");
            QMessageBox::critical(nullptr, title, message, QMessageBox::Ok, 0, 0);

            return;
        }

        if (status && !this->filterSubsystemModule->updateModuleData())
        {
            QMessageBox::critical(nullptr, "Error", "Failed to update module data", QMessageBox::Ok, 0, 0);
        }

        refresh();
    }
}

void FilterSystemTabPage::on_removePushButton_clicked()
{
    auto selectedItems = ui->protectedObjectsTableWidget->selectedItems();

    for (const auto& selectedItem : selectedItems)
    {
        int row = ui->protectedObjectsTableWidget->row(selectedItem);

        auto item = ui->protectedObjectsTableWidget->item(row, 0);

        ProtectedObject object;
        object.id = item->text().toUtf8().constData();

        if (!reg->deleteObjectToProtect(object))
        {
            QString title("Error - Registry");
            QString message("Failed to remove object from registry");
            QMessageBox::critical(nullptr, title, message, QMessageBox::Ok, 0, 0);

            return;
        }

        if (status && !this->filterSubsystemModule->updateModuleData())
        {
            QMessageBox::critical(nullptr, "Error", "Failed to update module data", QMessageBox::Ok, 0, 0);
        }

        ui->protectedObjectsTableWidget->removeRow(row);
    }
}



