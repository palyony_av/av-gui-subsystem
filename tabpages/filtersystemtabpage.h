#ifndef FILTERSYSTEMTABPAGE_H
#define FILTERSYSTEMTABPAGE_H

#include "tabwidget.h"

#include "../dialogs/filtersystemdialog.h"
#include "../registry/registry.h"
#include "../module/avDllModule.h"

namespace Ui {
class FilterSystemTabPage;
}

class FilterSystemTabPage : public TabWidget
{
    Q_OBJECT

public:
    FilterSystemTabPage(QWidget *parent = nullptr);
    ~FilterSystemTabPage();

private slots:
    void on_switchPushButton_clicked();
    void on_refreshPushButton_clicked();
    void on_addPushButton_clicked();
    void on_removePushButton_clicked();

private:
    void refresh();

private:
    Ui::FilterSystemTabPage *ui;
    bool status = false;
    Registry* reg;
    std::unique_ptr<AVDllModule> filterSubsystemModule;
};

#endif // FILTERSYSTEMTABPAGE_H
