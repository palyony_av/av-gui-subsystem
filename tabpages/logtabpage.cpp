#include <QJsonArray>

#include "../module/logDllModule.h"
#include "logtabpage.h"
#include "ui_logtabpage.h"

LogTabPage::LogTabPage(QWidget *parent) :
    TabWidget(parent),
    ui(new Ui::LogTabPage)
{
    ui->setupUi(this);

    ui->logTableWidget->setColumnCount(4);
    ui->logTableWidget->setHorizontalHeaderLabels(QStringList() << "ID" << "Event" << "Date" << "Time");
    ui->logTableWidget->verticalHeader()->hide();

    // Stretch the second column (1 - Event)
    ui->logTableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

    this->pLogModule = LogDllModule::buildModule(DLL_LOG_MODULE_PATH);
    if (this->pLogModule == nullptr)
    {
        QMessageBox::warning(nullptr, "AV::LogTabPage",
                             "The " DLL_LOG_MODULE_NAME " did not found");
    }

    refresh();
}

LogTabPage::~LogTabPage()
{
    delete ui;
}

void LogTabPage::refresh()
{
    ui->logTableWidget->clearContents();

    /* Check is Log dll is loaded */
    if (this->pLogModule == nullptr)
    {
        QMessageBox::warning(nullptr, "AV::LogTabPage::refresh",
                             "The " DLL_LOG_MODULE_NAME " did not found");
        return;
    }

    /* Get Json message with all log items */
    uint32_t usedSize = 0;
    uint32_t logSize = DEFAULT_LOG_SIZE;
    std::unique_ptr<char[]> psLog(new char[logSize]);

    uint32_t result = this->pLogModule->fGetAllLogEntries(psLog.get(), logSize, &usedSize);
    if (result == AV_ERRONEOUS_INPUT)
    {
        QMessageBox::warning(nullptr, "AV::LogTabPage::refresh",
                             "Could not make refresh due to unexpected error");
        return;
    }

    while(result == AV_NOT_ENOUGH_MEMORY)
    {
        logSize = usedSize;
        psLog.reset(new char[logSize]);
        usedSize = 0;
        result = this->pLogModule->fGetAllLogEntries(psLog.get(), logSize, &usedSize);
    }

    /* Convert Json message to Json array */
    QJsonDocument doc = QJsonDocument::fromJson(
                QByteArray(psLog.get(), static_cast<int>(usedSize))
                );

    /* Check validity of the Json array */
    if(doc.isNull() || !doc.isArray())
    {
        QMessageBox::warning(nullptr, "AV::LogTabPage::refresh",
                             "Invalid doc");
        return;
    }

    ui->logTableWidget->setRowCount(0);
    QJsonArray jMsgArr = doc.array();
    for (QJsonValueRef jItem : jMsgArr)
    {
        QJsonObject jObj = jItem.toObject();

        QJsonValue valueId    = jObj["id"];
        QJsonValue valueEvent = jObj["event"];
        QJsonValue valueDate  = jObj["date"];
        QJsonValue valueTime  = jObj["time"];

        if (valueId == QJsonValue::Undefined || valueEvent == QJsonValue::Undefined
                || valueDate == QJsonValue::Undefined || valueTime == QJsonValue::Undefined)
        {
            QMessageBox::about(nullptr, "AV::LogTabPage::refresh", "Value does not exist");
            continue;
        }

        int row = ui->logTableWidget->rowCount();

        ui->logTableWidget->insertRow(row);

        ui->logTableWidget->setItem(row, 0, new QTableWidgetItem(QString::number(valueId.toInt())));
        ui->logTableWidget->setItem(row, 1, new QTableWidgetItem(valueEvent.toString()));
        ui->logTableWidget->setItem(row, 2, new QTableWidgetItem(valueDate.toString()));
        ui->logTableWidget->setItem(row, 3, new QTableWidgetItem(valueTime.toString()));
    }
}

void LogTabPage::on_refreshPushButton_clicked()
{
    refresh();
}
