#ifndef LOGTABPAGE_H
#define LOGTABPAGE_H

#include "tabwidget.h"
#include "../module/logDllModule.h"

namespace Ui {
class LogTabPage;
}

class LogTabPage : public TabWidget
{
    Q_OBJECT

public:
    LogTabPage(QWidget *parent = nullptr);
    ~LogTabPage();

private:
    void refresh();

private slots:
    void on_refreshPushButton_clicked();

private:
    Ui::LogTabPage* ui;
    std::unique_ptr<LogDllModule> pLogModule;
};

#endif // LOGTABPAGE_H
