#include <QMessageBox>
#include <QErrorMessage>

#include "QStandardItemModel"
#include "QStandardItem"

#include "networkprotectiontabpage.h"
#include "ui_networkprotectiontabpage.h"
#include "../dialogs/networkprotectiondialog.h"
#include "../module/avDllModule.h"

#pragma comment(lib, "Ole32.lib")

NetworkProtectionTabPage::NetworkProtectionTabPage(QWidget *parent)
    : TabWidget(parent),
      ui(new Ui::NetworkProtectionTabPage),
      reg(nullptr)
{
    ui->setupUi(this);

    ui->rulesTableWidget->setColumnCount(6);
    ui->rulesTableWidget->setHorizontalHeaderLabels(QStringList() << "ID" << "Type" << "IP Address" << "Port" << "Protocol" << "Action");
    ui->rulesTableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->rulesTableWidget->verticalHeader()->hide();
    ui->rulesTableWidget->horizontalHeader()->hideSection(0);

    try
    {
        reg = Registry::getInstance();
    }
    catch (const std::exception& e)
    {
        QString title("Error - NetworkProtectionTabPage");
        QMessageBox::critical(nullptr, title, e.what(), QMessageBox::Ok, 0, 0);
        exit(1);
    }

    this->pNetworkModule = AVDllModule::buildModule(DLL_NETWORK_MODULE_PATH);
    if (this->pNetworkModule == nullptr)
    {
        QMessageBox::warning(nullptr, "AV::NetworkProtectionTabPage",
                             "The " DLL_NETWORK_MODULE_NAME " did not found");
    }

    refresh();
}

NetworkProtectionTabPage::~NetworkProtectionTabPage()
{
    delete ui;
}

void NetworkProtectionTabPage::refresh()
{
    ui->rulesTableWidget->setRowCount(0);

    auto rules = reg->getNetworkRules();

    for (auto const& it : *rules)
    {
        QString id         = QString::fromUtf8(it.id.c_str());
        QString type       = QString::fromUtf8(it.type.c_str());
        QString ip         = QString::fromUtf8(it.ip.c_str());
        QString port       = QString::number(it.port);
        QString protocol   = QString::fromUtf8(it.protocol.c_str());
        QString action     = QString::fromUtf8(it.action.c_str());

        int row = ui->rulesTableWidget->rowCount();

        ui->rulesTableWidget->insertRow(row);

        ui->rulesTableWidget->setItem(row, 0, new QTableWidgetItem(id));
        ui->rulesTableWidget->setItem(row, 1, new QTableWidgetItem(type));
        ui->rulesTableWidget->setItem(row, 2, new QTableWidgetItem(ip));
        ui->rulesTableWidget->setItem(row, 3, new QTableWidgetItem(port));
        ui->rulesTableWidget->setItem(row, 4, new QTableWidgetItem(protocol));
        ui->rulesTableWidget->setItem(row, 5, new QTableWidgetItem(action));
    }
}

void NetworkProtectionTabPage::on_switchPushButton_clicked()
{
    ui->statusLabel->setText("Process...");
    ui->statusLabel->setStyleSheet(QString("QLabel { color : %0; }").arg("orange"));

    if (this->pNetworkModule == nullptr)
    {
        QMessageBox::warning(nullptr, "AV::NetworkProtectionTabPage",
                             "The " DLL_NETWORK_MODULE_NAME " did not found");
        goto Exit;
    }

    if (this->bNetworkModuleEnabled)
    {
        if(!this->pNetworkModule->uninstallModule())
        {
            goto Exit;
        }

        this->bNetworkModuleEnabled = false;
    } else {
        if (!this->pNetworkModule->installModule())
        {
            goto Exit;
        }

        this->bNetworkModuleEnabled = true;
        this->pNetworkModule->updateModuleData();
    }

Exit:
    ui->statusLabel->setText(this->bNetworkModuleEnabled ? "ON" : "OFF");
    ui->statusLabel->setStyleSheet(QString("QLabel { color : %0; }").arg(
                                       this->bNetworkModuleEnabled ? "green" : "red")
                                   );
}

void NetworkProtectionTabPage::on_addPushButton_clicked()
{
    NetworkProtectionDialog dialog;

    if (dialog.exec() != QDialog::Accepted)
    {
        return;
    }

    if (!bNetworkModuleEnabled)
    {
        QMessageBox::warning(nullptr, "AV::NetworkProtectionTabPage",
                             "The network module did not switched on");
        return;
    }

    NetworkRule rule = dialog.getData();

    // Insert data to registry
    if (!reg->addNetworkRule(rule))
    {
        QString message("Failed to add network rule to registry");
        QMessageBox::critical(nullptr, "Error", message, QMessageBox::Ok, 0, 0);
        return;
    }

    if (this->pNetworkModule == nullptr)
    {
        QMessageBox::warning(nullptr, "AV::NetworkProtectionTabPage",
                             "The " DLL_NETWORK_MODULE_NAME " did not found");
    }

    refresh();
}

void NetworkProtectionTabPage::on_removePushButton_clicked()
{
    auto selectedItems = ui->rulesTableWidget->selectedItems();

    for (const auto& selectedItem : selectedItems)
    {
        int row = ui->rulesTableWidget->row(selectedItem);

        auto item = ui->rulesTableWidget->item(row, 0);

        NetworkRule rule;
        rule.id = item->text().toUtf8().constData();

        if (!reg->deleteNetworkRule(rule))
        {
            QString title("Error - Registry");
            QString message("Failed to remove network rule from registry");
            QMessageBox::critical(nullptr, title, message, QMessageBox::Ok, 0, 0);
            return;
        }

        if (!bNetworkModuleEnabled)
        {
            continue;
        }

        if (this->pNetworkModule == nullptr)
        {
            QMessageBox::warning(nullptr, "AV::NetworkProtectionTabPage",
                                 "The " DLL_NETWORK_MODULE_NAME " did not found");
        }

        ui->rulesTableWidget->removeRow(row);
    }

    refresh();
}

void NetworkProtectionTabPage::on_applyPushButton_clicked()
{
    if (this->bNetworkModuleEnabled)
    {
        this->pNetworkModule->updateModuleData();
    }
}
