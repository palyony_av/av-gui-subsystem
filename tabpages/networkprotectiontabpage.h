#ifndef NETWORKPROTECTIONTABPAGE_H
#define NETWORKPROTECTIONTABPAGE_H

#include "tabwidget.h"
#include "../dialogs/networkprotectiondialog.h"
#include "../registry/registry.h"
#include "../module/avDllModule.h"

namespace Ui {
class NetworkProtectionTabPage;
}

class NetworkProtectionTabPage : public TabWidget
{
    Q_OBJECT

public:
    NetworkProtectionTabPage(QWidget *parent = nullptr);
    ~NetworkProtectionTabPage();

private slots:
    void on_switchPushButton_clicked();
    void on_addPushButton_clicked();
    void on_removePushButton_clicked();

    void on_applyPushButton_clicked();

private:
    void refresh();

private:
    Ui::NetworkProtectionTabPage *ui;
    NetworkProtectionTabPage *networkProtectionDialog;
    Registry* reg;
    std::unique_ptr<AVDllModule> pNetworkModule;
    bool bNetworkModuleEnabled = false;
};

#endif // NETWORKPROTECTIONTABPAGE_H
