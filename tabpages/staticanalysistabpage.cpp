#include <QMessageBox>
#include <QErrorMessage>
#include <QFileDialog>
#include <QStandardItemModel>
#include <QStandardItem>

#include "staticAnalysisTabPage.h"
#include "ui_staticAnalysisTabPage.h"

StaticAnalysisTabPage::StaticAnalysisTabPage(QWidget *parent) :
    TabWidget(parent),
    ui(new Ui::StaticAnalysisTabPage)
{
    ui->setupUi(this);

    ui->staticAnalysisResultTable->setColumnCount(6);
    ui->staticAnalysisResultTable->setHorizontalHeaderLabels({"Path", "ClamAV", "YaraMalw", "YaraPack", "Sign", "ML"});
    ui->staticAnalysisResultTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);

    this->pStaticAnalysisRegistry = StaticAnalysisRegistry::build();
    if (this->pStaticAnalysisRegistry == nullptr)
    {
        QMessageBox::warning(nullptr, "AV::StaticAnalysisTabPage",
                             "Could not open HKLM\\" AV_STATIC_ANAL_REG_KEY " registry path");
    }

    this->pStaticAnalysisModule = AVDllModule::buildModule(DLL_STATIC_ANALYSIS_PATH);
    if (this->pStaticAnalysisModule == nullptr)
    {
        QMessageBox::warning(nullptr, "AV::StaticAnalysisTabPage",
                             "The " DLL_STATIC_ANALYSIS_PATH " did not found");
    }

    refresh();
}

StaticAnalysisTabPage::~StaticAnalysisTabPage()
{
    delete ui;
}

void StaticAnalysisTabPage::refresh()
{
    if (this->pStaticAnalysisRegistry == nullptr)
    {
        QMessageBox::warning(nullptr, "StaticAnalysisTabPage::refresh",
                             "Could not open registry key for static analyzer");
        return;
    }

    auto objects = this->pStaticAnalysisRegistry->getAnalyzeObjects();
    this->ui->staticAnalysisResultTable->setRowCount(0);
    for (const auto & obj : *objects)
    {
        int rowNum = this->ui->staticAnalysisResultTable->rowCount();

        this->ui->staticAnalysisResultTable->insertRow(rowNum);

        this->ui->staticAnalysisResultTable->setItem(rowNum, 0, new QTableWidgetItem(QString::fromStdString(obj.filePath)));

        /* The small bit in the status responsible for the processed state */
        /* Set ClamAV state */
        if ((obj.clamAVAnalyzeStatus & 1) == 0)
        {
            this->ui->staticAnalysisResultTable->setItem(rowNum, 1, new QTableWidgetItem(STATIC_ANAL_STATE_PROCESSING));
        } else {
            if ((obj.clamAVAnalyzeStatus >> 1) == STATIC_ANAL_MALWARE_STATE)
            {
                this->ui->staticAnalysisResultTable->setItem(rowNum, 1, new QTableWidgetItem(STATIC_ANAL_STATE_MALWARE));
            } else {
                this->ui->staticAnalysisResultTable->setItem(rowNum, 1, new QTableWidgetItem(STATIC_ANAL_STATE_BENIGN));
            }
        }

        /* Set YaraMalw state */
        if ((obj.yaraMalwAnalyzeStatus & 1) == 0)
        {
            this->ui->staticAnalysisResultTable->setItem(rowNum, 2, new QTableWidgetItem(STATIC_ANAL_STATE_PROCESSING));
        } else {
            if ((obj.yaraMalwAnalyzeStatus >> 1) == STATIC_ANAL_MALWARE_STATE)
            {
                this->ui->staticAnalysisResultTable->setItem(rowNum, 2, new QTableWidgetItem(STATIC_ANAL_STATE_MALWARE));
            } else {
                this->ui->staticAnalysisResultTable->setItem(rowNum, 2, new QTableWidgetItem(STATIC_ANAL_STATE_BENIGN));
            }
        }

        /* Set YaraPack state */
        if ((obj.yaraPackAnalyzeStatus & 1) == 0)
        {
            this->ui->staticAnalysisResultTable->setItem(rowNum, 3, new QTableWidgetItem(STATIC_ANAL_STATE_PROCESSING));
        } else {
            if ((obj.yaraPackAnalyzeStatus >> 1) == STATIC_ANAL_MALWARE_STATE)
            {
                this->ui->staticAnalysisResultTable->setItem(rowNum, 3, new QTableWidgetItem(STATIC_ANAL_STATE_MALWARE));
            } else {
                this->ui->staticAnalysisResultTable->setItem(rowNum, 3, new QTableWidgetItem(STATIC_ANAL_STATE_BENIGN));
            }
        }

        /* Set ClamAV state */
        if ((obj.signCheckStatus & 1) == 0)
        {
            this->ui->staticAnalysisResultTable->setItem(rowNum, 4, new QTableWidgetItem(STATIC_ANAL_STATE_PROCESSING));
        } else {
            if ((obj.signCheckStatus >> 1) == STATIC_ANAL_MALWARE_STATE)
            {
                this->ui->staticAnalysisResultTable->setItem(rowNum, 4, new QTableWidgetItem(STATIC_ANAL_STATE_MALWARE));
            } else {
                this->ui->staticAnalysisResultTable->setItem(rowNum, 4, new QTableWidgetItem(STATIC_ANAL_STATE_BENIGN));
            }
        }

        /* Set MLCheckStatus state */
        if ((obj.mlCheckStatus & 1) == 0)
        {
            this->ui->staticAnalysisResultTable->setItem(rowNum, 5, new QTableWidgetItem(STATIC_ANAL_STATE_PROCESSING));
        } else {
            if ((obj.signCheckStatus >> 1) == STATIC_ANAL_MALWARE_STATE)
            {
                this->ui->staticAnalysisResultTable->setItem(rowNum, 5, new QTableWidgetItem(STATIC_ANAL_STATE_MALWARE));
            } else if ((obj.mlCheckStatus >> 1) == STATIC_ANAL_BENIGN_STATE)
            {
                this->ui->staticAnalysisResultTable->setItem(rowNum, 5, new QTableWidgetItem(STATIC_ANAL_STATE_BENIGN));
            } else {
                this->ui->staticAnalysisResultTable->setItem(rowNum, 5, new QTableWidgetItem(STATIC_ANAL_STATE_ERROR));
            }
        }
    }
}

void StaticAnalysisTabPage::on_switchPushButton_clicked()
{
    ui->statusLabel->setText("Process...");
    ui->statusLabel->setStyleSheet(QString("QLabel { color : %0; }").arg("orange"));

    if (this->pStaticAnalysisModule == nullptr)
    {
        QMessageBox::warning(nullptr, "AV::StaticAnalysisTabPage",
                             "The " DLL_STATIC_ANALYSIS_PATH " did not found");
        goto Exit;
    }

    if (this->bStaticAnalysisEnabled)
    {
        if(!this->pStaticAnalysisModule->uninstallModule())
        {
            goto Exit;
        }

        this->bStaticAnalysisEnabled = false;
    } else {
        if (!this->pStaticAnalysisModule->installModule())
        {
            goto Exit;
        }

        this->bStaticAnalysisEnabled = true;
        this->pStaticAnalysisModule->updateModuleData();
    }

Exit:
    ui->statusLabel->setText(this->bStaticAnalysisEnabled ? "ON" : "OFF");
    ui->statusLabel->setStyleSheet(QString("QLabel { color : %0; }").arg(
                                       this->bStaticAnalysisEnabled ? "green" : "red")
    );
}

void StaticAnalysisTabPage::on_refreshPushButton_clicked()
{
    refresh();
}

void StaticAnalysisTabPage::on_scanFilePushButton_clicked()
{
    if (this->pStaticAnalysisRegistry == nullptr)
    {
        QMessageBox::warning(nullptr, "StaticAnalysisTabPage::on_scanFileButton_clicked",
                             "Could not open registry key for static analyzer");
        return;
    }

    if (this->pStaticAnalysisModule == nullptr)
    {
        QMessageBox::warning(nullptr, "AV::StaticAnalysisTabPage",
                             "The " DLL_STATIC_ANALYSIS_PATH " did not found");
        return;
    }

    QString filename = QFileDialog::getOpenFileName(this,
                                                    "Choose file to analyze",
                                                    QString(),
                                                    tr("All files (*.*)"));

    if (filename.size() == 0)
    {
        return;
    }

    StaticAnalysisRegistry::StaticAnalysisObject obj(
                filename.toUtf8().constData(),
                0,
                0,
                0,
                0,
                0
     );
    if (!this->pStaticAnalysisRegistry->addObjToAnalyze(obj))
    {
        return;
    }

    int rowNum = this->ui->staticAnalysisResultTable->rowCount();

    this->ui->staticAnalysisResultTable->insertRow(rowNum);

    this->ui->staticAnalysisResultTable->setItem(rowNum, 0, new QTableWidgetItem(filename));
    this->ui->staticAnalysisResultTable->setItem(rowNum, 1, new QTableWidgetItem(STATIC_ANAL_STATE_PROCESSING));
    this->ui->staticAnalysisResultTable->setItem(rowNum, 2, new QTableWidgetItem(STATIC_ANAL_STATE_PROCESSING));
    this->ui->staticAnalysisResultTable->setItem(rowNum, 3, new QTableWidgetItem(STATIC_ANAL_STATE_PROCESSING));
    this->ui->staticAnalysisResultTable->setItem(rowNum, 4, new QTableWidgetItem(STATIC_ANAL_STATE_PROCESSING));
    this->ui->staticAnalysisResultTable->setItem(rowNum, 5, new QTableWidgetItem(STATIC_ANAL_STATE_PROCESSING));

    this->pStaticAnalysisModule->updateModuleData();
}

void StaticAnalysisTabPage::on_deleteScanResult_clicked()
{
    if (this->pStaticAnalysisRegistry == nullptr)
    {
        QMessageBox::warning(nullptr, "StaticAnalysisTabPage::on_scanFileButton_clicked",
                             "Could not open registry key for sandbox analyzer");
        return;
    }

    if (this->pStaticAnalysisModule == nullptr)
    {
        QMessageBox::warning(nullptr, "AV::StaticAnalysisTabPage",
                             "The " DLL_CLOUD_SCAN_MODULE_PATH " did not found");
        return;
    }

    auto selectedItems = ui->staticAnalysisResultTable->selectedItems();

    for (const auto& selectedItem : selectedItems)
    {
        int row = selectedItem->row();

        if (!this->pStaticAnalysisRegistry->delObjToAnalyze(static_cast<uint32_t>(row)))
        {
            QMessageBox::warning(nullptr, "AV::StaticAnalysisTabPage",
                                 "Failed to remove network rule from registry");
            continue;
        }

        this->pStaticAnalysisModule->updateModuleData();

        ui->staticAnalysisResultTable->removeRow(row);
    }
}
