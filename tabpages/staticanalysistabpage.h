#ifndef STATICANALYSISTABPAGE_H
#define STATICANALYSISTABPAGE_H

#include "tabwidget.h"
#include "../module/avDllModule.h"
#include "../registry/staticAnalysisRegistry.h"

#define STATIC_ANAL_ERROR_STATE 2
#define STATIC_ANAL_MALWARE_STATE 1
#define STATIC_ANAL_BENIGN_STATE 0

#define STATIC_ANAL_STATE_ERROR "Error"
#define STATIC_ANAL_STATE_PROCESSING "Process"
#define STATIC_ANAL_STATE_MALWARE "Malware"
#define STATIC_ANAL_STATE_BENIGN "Benign"

namespace Ui {
class StaticAnalysisTabPage;
}

class StaticAnalysisTabPage : public TabWidget
{
    Q_OBJECT

public:
    StaticAnalysisTabPage(QWidget *parent = nullptr);
    ~StaticAnalysisTabPage();

private slots:
    void on_switchPushButton_clicked();
    void on_refreshPushButton_clicked();

    void on_scanFilePushButton_clicked();

    void on_deleteScanResult_clicked();

private:
    void refresh();

private:
    Ui::StaticAnalysisTabPage *ui;
    //StaticAnalysisDialog *staticAnalysisDialog;
    std::unique_ptr<AVDllModule> pStaticAnalysisModule;
    bool bStaticAnalysisEnabled = false;
    std::unique_ptr<StaticAnalysisRegistry> pStaticAnalysisRegistry;
};

#endif // STATICANALYSISTABPAGE_H
