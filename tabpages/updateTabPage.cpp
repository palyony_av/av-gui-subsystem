#define _CRT_SECURE_NO_WARNINGS

#include "updateTabPage.h"
#include "ui_updateTabPage.h"

UpdateTabPage::UpdateTabPage(QWidget *parent) :
    TabWidget(parent),
    ui(new Ui::UpdateTabPage)
{
    ui->setupUi(this);

    ui->updateResultTable->setColumnCount(3);
    ui->updateResultTable->setHorizontalHeaderLabels({"Module name", "State", "Version"});
    ui->updateResultTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);

    this->pUpdaterModuleRegistry = UpdaterRegistry::build();
    if (this->pUpdaterModuleRegistry == nullptr)
    {
        QMessageBox::warning(nullptr, "AV::UpdateTabPage",
                             "Could not open HKLM\\" AV_UPDATER_REG_KEY " registry path");
    }

    this->pUpdaterModule = AVDllModule::buildModule(DLL_UPDATER_MODULE_PATH);
    if (this->pUpdaterModule == nullptr)
    {
        QMessageBox::warning(nullptr, "AV::UpdateTabPage",
                             "The " DLL_UPDATER_MODULE_PATH " did not found");
    }

    this->refresh();
}

UpdateTabPage::~UpdateTabPage()
{
    delete ui;
}

void UpdateTabPage::on_scanDownloadButton_clicked()
{
    if (this->pUpdaterModuleRegistry == nullptr)
    {
        QMessageBox::warning(nullptr, "UpdateTabPage::on_scanDownloadButton_clicked",
                             "Could not open registry key for Updater module");
        return;
    }

    if (this->pUpdaterModule == nullptr)
    {
        QMessageBox::warning(nullptr, "UpdateTabPage::on_scanDownloadButton_clicked",
                             "The " DLL_UPDATER_MODULE_PATH " did not found");
        return;
    }

    if (this->bUpdaterModuleEnabled == false)
    {
        QMessageBox::warning(nullptr, "UpdateTabPage::on_scanDownloadButton_clicked",
                             "The module Updater is not enabled");
    }

    this->pUpdaterModule->updateModuleData();
}

void UpdateTabPage::on_refreshButton_clicked()
{
    this->refresh();
}

void UpdateTabPage::on_switchButton_clicked()
{
    ui->statusLabel->setText("Process...");
    ui->statusLabel->setStyleSheet(QString("QLabel { color : %0; }").arg("orange"));

    if (this->pUpdaterModule == nullptr)
    {
        QMessageBox::warning(nullptr, "AV::UpdateTabPage",
                             "The " DLL_NETWORK_MODULE_NAME " did not found");
        goto Exit;
    }

    if (this->bUpdaterModuleEnabled)
    {
        if(!this->pUpdaterModule->uninstallModule())
        {
            goto Exit;
        }

        this->bUpdaterModuleEnabled = false;
    } else {
        if (!this->pUpdaterModule->installModule())
        {
            goto Exit;
        }

        this->bUpdaterModuleEnabled = true;
    }

Exit:
    ui->statusLabel->setText(this->bUpdaterModuleEnabled ? "ON" : "OFF");
    ui->statusLabel->setStyleSheet(QString("QLabel { color : %0; }").arg(this->bUpdaterModuleEnabled ? "green" : "red"));
}

void UpdateTabPage::refresh()
{
    if (this->pUpdaterModuleRegistry == nullptr)
    {
        QMessageBox::warning(nullptr, "UpdateTabPage::refresh",
                             "Could not open registry key for updater");
        return;
    }

    auto objects = this->pUpdaterModuleRegistry->getAnalyzeObjects();
    this->ui->updateResultTable->setRowCount(0);
    for (const auto & obj : *objects)
    {
        int rowNum = this->ui->updateResultTable->rowCount();

        this->ui->updateResultTable->insertRow(rowNum);

        this->ui->updateResultTable->setItem(rowNum, 0, new QTableWidgetItem(QString::fromStdString(obj.path)));

        switch (obj.state)
        {
        case UpdaterRegistry::AVModuleState::updated:
        {
            this->ui->updateResultTable->setItem(rowNum, 1, new QTableWidgetItem(UPDATER_STATE_UPDATED));
        } break;
        case UpdaterRegistry::AVModuleState::downloaded:
        {
            this->ui->updateResultTable->setItem(rowNum, 1, new QTableWidgetItem(UPDATER_STATE_DOWNLOADED));
        } break;
        case UpdaterRegistry::AVModuleState::waitForDownloading:
        {
            this->ui->updateResultTable->setItem(rowNum, 1, new QTableWidgetItem(UPDATER_STATE_WAIT));
        } break;
        }

        this->ui->updateResultTable->setItem(rowNum, 2, new QTableWidgetItem(QString::fromStdString(obj.version)));
    }
}

void UpdateTabPage::on_installUpdatesButton_clicked()
{
    constexpr auto PIPE_BUFFERLENGTH = MAX_PATH;

    HANDLE hRdSlave = nullptr, hWrMaster = nullptr;
    HANDLE hDuplWrMaster = nullptr;
    STARTUPINFOA startInfo;
    PROCESS_INFORMATION prcInfo;
    SECURITY_ATTRIBUTES secAttr;
    char pSlaveCmdLine[MAX_PATH] = { '\0' };
    BOOL bReturn;

    secAttr.bInheritHandle = TRUE;
    secAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
    secAttr.lpSecurityDescriptor = nullptr;

    GetStartupInfoA(&startInfo);

    bReturn = CreatePipe(&hRdSlave, &hWrMaster, &secAttr, PIPE_BUFFERLENGTH);
    DuplicateHandle(GetCurrentProcess(), hWrMaster, GetCurrentProcess(), &hDuplWrMaster, DUPLICATE_SAME_ACCESS, FALSE, DUPLICATE_SAME_ACCESS);
    CloseHandle(hWrMaster);

    // preparing the command line for the child process.
    std::sprintf(pSlaveCmdLine, "%p", hRdSlave);

    bReturn = CreateProcessA(UPDATER_APP_PATH, pSlaveCmdLine, nullptr, nullptr, TRUE, NORMAL_PRIORITY_CLASS, nullptr, nullptr, &startInfo, &prcInfo);
    if (!bReturn)
    {
        QMessageBox::warning(nullptr, "UpdateTabPage::on_installUpdatesButton_clicked",
                             "Could not create process");
        return;
    }

    CloseHandle(prcInfo.hThread);
    CloseHandle(prcInfo.hProcess);

    auto objects = this->pUpdaterModuleRegistry->getAnalyzeObjects();
    if (objects == nullptr)
    {
        QMessageBox::warning(nullptr, "UpdateTabPage::on_installUpdatesButton_clicked",
                             "Could not get object from registry");
        return;
    }

    uint16_t writeCycleNum = static_cast<uint16_t>(objects->size());
    DWORD dwWrite = sizeof(uint16_t);
    BOOL bRet;
    bRet = WriteFile(hDuplWrMaster, &writeCycleNum, sizeof(uint16_t), &dwWrite, nullptr);
    if (bRet == FALSE || dwWrite != sizeof(uint16_t))
    {
        QMessageBox::warning(nullptr, "UpdateTabPage::on_installUpdatesButton_clicked",
                             "Could not write data to child");
        return;
    }

    for (uint16_t i = 0; i < writeCycleNum; i += 1)
    {
        uint16_t fromStrSize = static_cast<uint16_t>(objects->at(i).module.size() + 1);
        uint16_t toStrSize = static_cast<uint16_t>(objects->at(i).path.size() + 1);

        bRet = WriteFile(hDuplWrMaster, &fromStrSize, sizeof(uint16_t), &dwWrite, nullptr);
        if (bRet == FALSE || dwWrite != sizeof(uint16_t))
        {
            return;
        }

        bRet = WriteFile(hDuplWrMaster, objects->at(i).module.c_str(), fromStrSize, &dwWrite, nullptr);
        if (bRet == FALSE || dwWrite != fromStrSize)
        {
            return;
        }

        bRet = WriteFile(hDuplWrMaster, &toStrSize, sizeof(uint16_t), &dwWrite, nullptr);
        if (bRet == FALSE || dwWrite != sizeof(uint16_t))
        {
            return;
        }

        bRet = WriteFile(hDuplWrMaster, objects->at(i).path.c_str(), toStrSize, &dwWrite, nullptr);
        if (bRet == FALSE || dwWrite != toStrSize)
        {
            return;
        }
    }

    CloseHandle(hDuplWrMaster);
    CloseHandle(hRdSlave);

    this->pUpdaterModuleRegistry->setDownloadedToUpdated();

    QApplication::quit();
}
