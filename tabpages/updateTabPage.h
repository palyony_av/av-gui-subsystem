#ifndef UPDATETABPAGE_H
#define UPDATETABPAGE_H

#include <tabwidget.h>
#include "registry/updaterRegistry.h"
#include "module/avDllModule.h"

#define UPDATER_STATE_UPDATED "Updated"
#define UPDATER_STATE_DOWNLOADED "Downloaded"
#define UPDATER_STATE_WAIT "Wait"

namespace Ui {
class UpdateTabPage;
}

class UpdateTabPage : public TabWidget
{
    static constexpr auto UPDATER_APP_PATH = ".\\updater.exe";

    Q_OBJECT

public:
    explicit UpdateTabPage(QWidget *parent = nullptr);
    ~UpdateTabPage();

private slots:

    void on_scanDownloadButton_clicked();

    void on_refreshButton_clicked();

    void on_installUpdatesButton_clicked();

    void on_switchButton_clicked();

private:
    void refresh();

    Ui::UpdateTabPage *ui;
    bool bUpdaterModuleEnabled = false;
    std::unique_ptr<UpdaterRegistry> pUpdaterModuleRegistry;
    std::unique_ptr<AVDllModule> pUpdaterModule;
};

#endif // UPDATETABPAGE_H
