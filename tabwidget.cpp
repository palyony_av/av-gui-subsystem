#include "tabwidget.h"

#include "ui_mainwindow.h"

TabBar::TabBar(QWidget* parent)
    : QTabBar(parent)
{
    setIconSize(QSize(18, 18));
}

QSize TabBar::tabSizeHint(int) const
{
    return QSize(150, 30);
}

void TabBar::paintEvent(QPaintEvent *)
{
    QStylePainter p(this);
    for (int index = 0; index < count(); index++)
    {
        QStyleOptionTabV3 tab;
        initStyleOption(&tab, index);
        QIcon tempIcon = tab.icon;
        QString tempText = tab.text;
        tab.icon = QIcon();
        tab.text = QString();
        p.drawControl(QStyle::CE_TabBarTab, tab);
        QPainter painter;
        painter.begin(this);
        QRect tabrect = tabRect(index);
        tabrect.adjust(0, 8, 0, -8);
        painter.drawText(tabrect, Qt::AlignBottom | Qt::AlignHCenter, tempText);
        tempIcon.paint(&painter, 0, tabrect.top(), tab.iconSize.width(), tab.iconSize.height(), Qt::AlignTop | Qt::AlignHCenter);
        painter.end();
    }
}

TabWidget::TabWidget(QWidget *parent) :
    QTabWidget(parent)
{
    setTabBar(new TabBar());
    setTabPosition(QTabWidget::West);
}


