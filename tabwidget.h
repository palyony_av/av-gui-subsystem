#ifndef TABWIDGET_H
#define TABWIDGET_H

#include <QTabBar>
#include <QStylePainter>
#include <QStyleOptionTabV3>
#include <QPainter>
#include <QIcon>
#include <QString>

class TabBar : public QTabBar
{
public:
    explicit TabBar(QWidget* parent = nullptr);

protected:
    QSize tabSizeHint(int) const;
    void paintEvent(QPaintEvent *);
};

class TabWidget : public QTabWidget
{
public:
    explicit TabWidget(QWidget *parent = nullptr);

    enum Tab
    {
        FilterSubsystem,
        StaticAnalysis,
        RealTimeProtection,
        CloudScan,
        Update,
        NetworkProtection,
        Log
    };
};

#endif // TABWIDGET_H
